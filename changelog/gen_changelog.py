#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import sys

sys.path.insert(0, os.path.join(os.path.dirname(__file__), '../hooks'))

import common  # noqa:E402
import re  # noqa:E402
import argparse  # noqa:E402
import textwrap  # noqa:E402


# Check commit structure
TITLE_PATTERN_REGEX = r'(?P<type>\w+)' \
                      r'\((?P<scope>\S+)\):(?P<subject> [A-z].*)'


def gitlog(_rev, _rev2, options=''):
    command = 'git log --first-parent ' + options + ' ' + _rev + '..' + _rev2
    result = common.execute_command(command)

    if result.status == 0:
        return result.out.decode()

    raise Exception('Error executing "%s"', command)


def gen_log(_rev, _rev2, patch):
    if patch:
        difflog = gitlog(_rev, _rev2, '--pretty=format:%H')
        if difflog == '':
            exit(0)
        commits_range = difflog.split('\n')
        # Take the last element of the last row
        parent = commits_range[-1].split(' ')[-1]
        difflog = gitlog(parent, _rev2)
    else:
        difflog = gitlog(_rev, _rev2, '--pretty=format:%P')
        if difflog == '':
            exit(0)
        # Split rows
        commits_range = difflog.split('\n')
        # Take the last element of the last row
        parent = commits_range[-1].split(' ')[-1]
        difflog = gitlog(_rev, parent)

    commits = re.split(r'commit [a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9][a-f0-9]', difflog)

    changelog = dict()

    for commit in commits:

        found_commit = False
        commit_description = ""
        commit_type = ""
        commit_scope = ""
        commit_subject = ""

        regex_indent = re.compile('^    ')
        regex_see_mr = re.compile('^See merge request.*')
        regex_merge_branch = re.compile('^Merge branch.*')
        regex_close_bug = re.compile('^[Cc]loses?.*#.*')
        regex_howto = re.compile('^## How to test.*')
        regex_fix_symbols = re.compile('(::[A-z:]*) ')

        was_previous_line_empty = False
        for line in commit.splitlines():

            if found_commit:

                #  Strip some text...
                description_line = re.sub(regex_indent, '', line)
                empty_line = len(description_line) == 0
                description_line = re.sub(regex_see_mr, '', description_line)
                description_line = re.sub(regex_close_bug, '', description_line)
                description_line = re.sub(regex_merge_branch, '', description_line)
                description_line = re.sub(regex_fix_symbols, r'`\1`', description_line)

                if re.match(regex_howto, description_line):
                    # Simply stops there if someone forgot the usual gitlab description part
                    break
                if len(description_line):
                    commit_description += description_line + '\n'
                    was_previous_line_empty = False
                elif empty_line:
                    if not was_previous_line_empty:
                        commit_description += '\n'
                        was_previous_line_empty = True
                else:
                    was_previous_line_empty = True

            else:
                title_pattern = re.compile(TITLE_PATTERN_REGEX)
                title_match = title_pattern.search(line)

                if title_match:
                    commit_scope = title_match.group('scope')
                    if commit_scope == 'master':
                        continue

                    commit_type = title_match.group('type')
                    commit_subject = title_match.group('subject').strip(' ')
                    # Ensure the scope has a proper line ending
                    commit_subject = commit_subject.strip('\n')

                    if commit_subject[-1] != '.':
                        commit_subject += '.'

                    # Force upper case on the first letter
                    commit_subject = commit_subject[0].upper() + commit_subject[1:]

                    commit_subject = '*' + commit_subject + '*'
                    commit_subject += '\n'

                    if commit_type == 'merge':
                        commit_type = 'feat'
                    found_commit = True

        if found_commit:

            if commit_type not in changelog:
                changelog[commit_type] = []

            changelog[commit_type].append([commit_scope, commit_subject, commit_description])

    try:
        repo_name = open(".sight", 'r').read()
    except FileNotFoundError:
        repo_name = open(".sight-deps", 'r').read()
    repo_name = repo_name.strip('\n')

    formatted_changelog = '# ' + repo_name + ' ' + _rev2 + '\n\n'

    for commit_type, entries in changelog.items():

        if commit_type in ['feat']:
            formatted_changelog += '## New features:\n\n'
        elif commit_type in ['fix']:
            formatted_changelog += '## Bug fixes:\n\n'
        elif commit_type in ['refactor']:
            formatted_changelog += '## Refactor:\n\n'
        elif commit_type in ['enh']:
            formatted_changelog += '## Enhancement:\n\n'
        else:
            continue

        # Sort list by scope, use a lambda to sort with case-insensitivity the first element.
        sorted_entries = sorted(entries, key=lambda k: (k[0].lower()))
        previous_entry = ''

        for entry in sorted_entries:
            # Check first if entry is the same as the previous one.
            # If not add a new entry title.
            if previous_entry.lower() != str(entry[0]).lower():
                formatted_changelog += "### " + entry[0] + '\n\n'
                previous_entry = entry[0]
            if not re.match(r' ?Into', entry[1]):
                formatted_changelog += entry[1]
            formatted_changelog += entry[2]

    print(formatted_changelog)


parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='Generate a changelog for a Sight repository.',
    epilog=textwrap.dedent('The script works on a range of commits and can generate in two ways:\n'
                           '\n'
                           '  When releasing a major or minor version, it will consider that the commit where the more'
                           '  recent tag is placed is the merge from dev to master. It will go up in the history in the'
                           ' dev branch and generate the changelog with all its commits since the older tag.\n'
                           '  This is the default mode.'
                           '\n'
                           '  When releasing a patch, it will generate the changelog with all commits in the specified'
                           ' range in the current branch, normally the master branch.\n'
                           ' You must use the -p switch to use this mode.'
                           )
)

parser.add_argument('path',
                    nargs='*',
                    help='Git path, can be a commit or two commits.')

parser.add_argument('-p', '--patch',
                    action="store_true",
                    help='Generate the changelog for a patch release.')

args = parser.parse_args()

try:
    rev1 = args.path[0]
    rev2 = args.path[1]
except IndexError:
    parser.print_usage()
    exit(1)

gen_log(rev1, rev2, args.patch)
