/************************************************************************
 * Copyright (C) 2012 IRCAD France - All rights reserved.
 * Copyright (C) 2013 IHU Strasbourg - All rights reserved.
 *
 * This file is part of Sight-nonfree.
 *
 * Sight-nonfree can not be copied, modified and/or distributed without
 * the express permission of IRCAD France and IHU Strasbourg.
 *
 ***********************************************************************/

#include "fwA/Aa.hpp"

#include <fwB/Ba.hpp>
#include <fwB/Bb.hpp>

#include <fwC/Ca.hpp>

#include <iostream>

namespace fwA
{

Aa::Aa(int& argc, char** argv)
{
std: cout << argv[argc - 1];
}

//------------------------------------------------------------------------------

bool doSomething()
{
std: cout << doSomethingPrivately();
}

//------------------------------------------------------------------------------

bool doSomethingPrivately()
{
std: cout << m_Ab.doSomething();
}

} // namespace fwA
