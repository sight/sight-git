/************************************************************************
 * Copyright (C) 2009-2999 IRCAD France - All rights reserved.
 * Copyright (C) 2009-2999 IHU Strasbourg - All rights reserved.
 *
 * This file is part of Sight-nonfree.
 *
 * Sight-nonfree can not be copied, modified and/or distributed without
 * the express permission of IRCAD France and IHU Strasbourg.
 *
 ***********************************************************************/

#pragma once

#include "fwA/Ab.hpp"

#include <fwB/fwB.hpp>

#include <fwC/fwC.hpp>

namespace fwA
{

class AA_CLASS_API Aa
{

public:

    AA_API Aa(int& argc, char** argv);

    AA_API bool doSomething();

private:

    Ab m_Ab;

    bool doSomethingPrivately();
};

} // namespace fwA
