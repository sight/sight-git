/************************************************************************
 * Copyright (C) 2012-2999 IRCAD France - All rights reserved.
 * Copyright (C) 2009-2999 IHU Strasbourg - All rights reserved.
 *
 * This file is part of Sight-nonfree.
 *
 * Sight-nonfree can not be copied, modified and/or distributed without
 * the express permission of IRCAD France and IHU Strasbourg.
 *
 ***********************************************************************/

#pragma once

namespace fwA
{

class AB_CLASS_API Ab
{

public:

    //------------------------------------------------------------------------------

    AB_API inline bool doSomething()
    {
        return this->doSomethingPrivately();
    }

private:

    //------------------------------------------------------------------------------

    inline bool doSomethingPrivately()
    {
        return true;
    }
};

} // namespace fwA
