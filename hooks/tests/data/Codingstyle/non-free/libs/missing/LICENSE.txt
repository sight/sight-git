Sight-nonfree is: Copyright (C) 2009-2020 IRCAD France - All rights reserved.
                  Copyright (C) 2012-2020 IHU Strasbourg - All rights reserved.
Contact: sds.ircad@ircad.fr

Sight-nonfree can not be copied, modified and/or distributed without
the express permission of IRCAD France and IHU Strasbourg.
