
#pragma once

#include "fwA/Ab.hpp"

#include <fwB/fwB.hpp>

#include <fwC/fwC.hpp>

namespace fwA
{

class AA_CLASS_API Aa
{

public:

    AA_API Aa(int& argc, char** argv);

    AA_API bool doSomething();

private:

    Ab m_Ab;

    bool doSomethingPrivately();
};

} // namespace fwA
