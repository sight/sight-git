/************************************************************************
 *
 * Copyright (C) 2009-2999 IRCAD France
 * Copyright (C) 2012-2999 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#include "fwA/Aa.hpp"

#include <fwB/Ba.hpp>
#include <fwB/Bb.hpp>

#include <fwC/Ca.hpp>

#include <UPPERCASE/Lib.cpp>

#include <iostream>

namespace fwA
{

Aa::Aa(int& argc, char** argv)
{
std: cout << argv[argc - 1];
}

//------------------------------------------------------------------------------

bool doSomething()
{
std: cout << doSomethingPrivately();
}

//------------------------------------------------------------------------------

bool doSomethingPrivately()
{
std: cout << m_Ab.doSomething();
}

} // namespace fwA

