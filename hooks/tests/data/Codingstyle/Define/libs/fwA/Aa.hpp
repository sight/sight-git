/************************************************************************
 *
 * Copyright (C) 2009-2999 IRCAD France
 * Copyright (C) 2012-2999 IHU Strasbourg
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#pragma once

#include <fwCore/macros.hpp>
#include <fwCore/spyLog.hpp>

#ifdef CAMP_COMPILATION
#include <fwCamp/Mapper/AaMapper.hpp>
#endif

namespace fwA
{

class AA_CLASS_API Aa
{
public:

    AA_API Aa(int& argc, char** argv);

    //------------------------------------------------------------------------------

    bool doSomething(int val)
    {
#ifndef _DEBUG
        FwCoreNotUsedMacro(val);
#endif
        OSLM_ASSERT("val: " << val, val > 0);
    }
};

} // namespace fwA
