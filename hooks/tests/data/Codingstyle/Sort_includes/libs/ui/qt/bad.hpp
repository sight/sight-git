/************************************************************************
 *
 * Copyright (C) 2009-2999 IRCAD France
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#pragma once

#include <random/library/a.hpp>
#include <random/library/b.hpp>

#include <core/data/data.hpp>

#include <ui/base/b.hpp>
#include <ui/qt/a.hpp>

#include <memory>
#include <vector>

namespace ui::base
{

class AB_CLASS_API Ab
{
public:

    //------------------------------------------------------------------------------

    AB_API inline bool doSomething()
    {
        return this->doSomethingPrivately();
    }

private:

    //------------------------------------------------------------------------------

    inline bool doSomethingPrivately()
    {
        return true;
    }
};

} // namespace ui::base
