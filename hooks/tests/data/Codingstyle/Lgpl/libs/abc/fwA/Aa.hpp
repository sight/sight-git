/************************************************************************
 *
 * Copyright (C) 2009-2000 IRCAD France
 *
 * This file is part of Sight.
 *
 * Sight is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Sight is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with Sight. If not, see <https://www.gnu.org/licenses/>.
 *
 ***********************************************************************/

#ifndef __FWA_AA_HPP__
#define __FWA_AA_HPP__

#include "fwA/Ab.hpp"

#include <fwB/fwB.hpp>

#include <fwC/fwC.hpp>

namespace fwA
{

class AA_CLASS_API Aa
{

public:

    AA_API Aa(int& argc, char** argv);

    AA_API bool doSomething();

private:

    Ab m_Ab;

    bool doSomethingPrivately();
};

} // namespace fwA

#endif // __FWA_AA_HPP__