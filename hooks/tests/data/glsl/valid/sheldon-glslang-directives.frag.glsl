#version 430

//-----------------------------------------------------------------------------
// glslang

// Note: the extra spaces in some of the "// sheldon: " comments are added purposefully to test the regex

// sheldon:glslang require-exclusive(SIGHT_PATH_A, SIGHT_PATH_B)
// sheldon: glslang discard(SIGHT_DO_FOO)
// sheldon : glslang discard(SIGHT_DO_BAR)
// sheldon :glslang discard(SIGHT_DO_ZOG)
// sheldon    :  glslang require-exclusive(SIGHT_PATH_C    ,     SIGHT_PATH_D)

//-----------------------------------------------------------------------------
// Outputs

layout(location = 0) out vec4 out_fragment_colour;

//-----------------------------------------------------------------------------
// Sanity checks

#if defined(SIGHT_PATH_A)

    #if defined(SIGHT_PATH_B)
        #error Invalid configuration. Expected at most one of SIGHT_PATH_A, SIGHT_PATH_B.
    #endif

#elif defined(SIGHT_PATH_B)

    #if defined(SIGHT_PATH_A)
        #error Invalid configuration. Expected at most one of SIGHT_PATH_A, SIGHT_PATH_B.
    #endif

#else
    #error Missing preprocessor define. Expected SIGHT_PATH_A or SIGHT_PATH_B.
#endif


#if defined(SIGHT_PATH_C)

    #if defined(SIGHT_PATH_D)
        #error Invalid configuration. Expected at most one of SIGHT_PATH_C, SIGHT_PATH_D.
    #endif

#elif defined(SIGHT_PATH_D)

    #if defined(SIGHT_PATH_C)
        #error Invalid SIGHT_PATH_C. Expected at most one of SIGHT_PATH_C, SIGHT_PATH_D.
    #endif

#else
    #error Missing preprocessor define. Expected SIGHT_PATH_A or SIGHT_PATH_D.
#endif

#ifdef SIGHT_DO_FOO
    #error Should not be defined.
#endif

#ifdef SIGHT_DO_BAR
    #error Should not be defined.
#endif

#ifdef SIGHT_DO_ZOG
    #error Should not be defined.
#endif

#define SIGHT_DO_FOO(x)
#define SIGHT_DO_BAR(x)
#define SIGHT_DO_ZOG(x)

//-----------------------------------------------------------------------------
// Main

void main()
{
    out_fragment_colour = vec4(1.f);
}
