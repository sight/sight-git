#version 460

//-----------------------------------------------------------------------------
// Outputs

layout(location = 0) out vec4 out_fragment_colour;

//-----------------------------------------------------------------------------
// Sanity checks

// By default, all of SIGHT_PATH_A, SIGHT_PATH_B, SIGHT_PATH_C, SIGHT_PATH_D are gathered by sheldon's glslang hook.
// Because they are supposed to be mutually exclusive 2 by 2, the compilation fails.
// To register mutually exlusive preprocessor defines, use "sheldon:glslang require-exclusive(...)"
// See it live in data/glsl/valid/sheldon-glslang-directives.frag.glsl.

#if defined(SIGHT_PATH_A)

    #if defined(SIGHT_PATH_B)
        #error Invalid configuration. Expected at most one of SIGHT_PATH_A, SIGHT_PATH_B.
    #endif

#elif defined(SIGHT_PATH_B)

    #if defined(SIGHT_PATH_A)
        #error Invalid configuration. Expected at most one of SIGHT_PATH_A, SIGHT_PATH_B.
    #endif

#else
    #error Missing preprocessor define. Expected SIGHT_PATH_A or SIGHT_PATH_B.
#endif

#if defined(SIGHT_PATH_C)

    #if defined(SIGHT_PATH_D)
        #error Invalid configuration. Expected at most one of SIGHT_PATH_C, SIGHT_PATH_D.
    #endif

#elif defined(SIGHT_PATH_D)

    #if defined(SIGHT_PATH_C)
        #error Invalid SIGHT_PATH_C. Expected at most one of SIGHT_PATH_C, SIGHT_PATH_D.
    #endif

#else
    #error Missing preprocessor define. Expected SIGHT_PATH_A or SIGHT_PATH_D.
#endif

// Similarly, internal #defines that are guarded like below are also gathered by the hook.
// For example, the following #ifdef guard against a developper error (having one of these #defined should not happen),
// therefore, attempting to compile a shader with any of SIGHT_DO_FOO, SIGHT_DO_BAR, SIGHT_DO_ZOG makes no sense.
// To register #defines the hook must not add on its own, use "sheldon: discard(...)"
// See it live in data/glsl/valid/sheldon-glslang-directives.frag.glsl.

#ifdef SIGHT_DO_FOO
    #error Should not be defined.
#endif

#ifdef SIGHT_DO_BAR
    #error Should not be defined.
#endif

#ifdef SIGHT_DO_ZOG
    #error Should not be defined.
#endif

#define SIGHT_DO_FOO(x)
#define SIGHT_DO_BAR(x)
#define SIGHT_DO_ZOG(x)

//-----------------------------------------------------------------------------
// Main

void main()
{
    out_fragment_colour = vec4(1.f);
}
