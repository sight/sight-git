#version 460

uniform mat3 u_worldViewProj;
in vec4 vertex;

void main()
{
    gl_Position = u_worldViewProj * vertex;
}
