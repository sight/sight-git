#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import unittest

import common


class TestCommon(unittest.TestCase):
    def test_get_repo_root(self):
        # Be verbose by default
        common.g_trace = True

        repo_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), '..', '..')
        repo_path = os.path.normpath(os.path.abspath(repo_path))

        self.assertRaises(FileNotFoundError, common.set_repo_root, os.path.split(repo_path)[0])

        # Check result
        common.set_repo_root(repo_path)
        self.assertEqual(repo_path, common.get_repo_root())

    def test_get_license(self):

        this_file_path = os.path.dirname(os.path.realpath(__file__))
        common.set_repo_root(this_file_path)

        # Two entities
        file = os.path.join(this_file_path, 'data/Codingstyle/non-free/libs/abc/fwA/Aa.hpp')
        license = common.get_license(file)
        self.assertEqual(license, [False, 'Sight-nonfree', 'IRCAD France', 'IHU Strasbourg'])

        # One entity
        file = os.path.join(this_file_path, 'data/Codingstyle/non-free/libs/abc/fwB/Ba.hpp')
        license = common.get_license(file)
        self.assertEqual(license, [False, 'Sight-nonfree', 'IRCAD France'])

        # LGPL
        file = os.path.join(this_file_path, 'data/Codingstyle/Header_guards_forgotten/libs/abc/fwC/Ca.hpp')
        license = common.get_license(file)
        self.assertEqual(license, [True, 'Sight', 'IRCAD France', 'IHU Strasbourg'])

        # Missing file returns private license... we don't really care about this case
        file = os.path.join(this_file_path, 'data/Codingstyle/Formatted/libs/abc/fwB/missing.hpp')
        self.assertRaises(FileNotFoundError, common.get_license, file)

        # File outside the repository
        file = os.path.join(this_file_path, '../../../')
        self.assertRaises(RuntimeError, common.get_license, file)

        # Unable to find a license file
        common.set_repo_root(os.path.join(this_file_path, 'data/Codingstyle/Sort_includes/lib_without_licence'))

        file = os.path.join(this_file_path, 'data/Codingstyle/Sort_includes/lib_without_licence/core/data/data.hpp')
        self.assertRaises(RuntimeError, common.get_license, file)


if __name__ == '__main__':
    unittest.main()
