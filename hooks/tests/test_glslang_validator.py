#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import unittest

import glslang_validator
import common


class TestGlslangValidator(unittest.TestCase):
    def test_glslang_invalid_glsl(self):
        common.g_trace = True

        invalid_files = [
            '/data/glsl/invalid/ColorFormats.glsl',
            '/data/glsl/invalid/NormalsDisplay_GP.glsl',
            '/data/glsl/invalid/RayTracedVolume_FP.glsl',
            '/data/glsl/invalid/Render_VP.glsl',
            '/data/glsl/invalid/sheldon-glslang-directives.frag.glsl'
        ]

        dir_path = os.path.dirname(os.path.realpath(__file__))
        for file in invalid_files:

            file_path = common.file_on_disk(dir_path + file)
            unsupported_version = glslang_validator.check_glsl_version(file_path)
            self.assertTrue(unsupported_version, 'Unsupported version detected as supported.')

            file_path = common.file_on_disk(dir_path + file)
            invalid_glsl = glslang_validator.glslang_validator(file_path)
            self.assertTrue(invalid_glsl, 'Invalid GLSL detected as valid.')

    def test_valid_glsl(self):
        common.g_trace = True

        valid_files = [
            '/data/glsl/valid/ColorFormats.glsl',
            '/data/glsl/valid/NormalsDisplay_GP.glsl',
            '/data/glsl/valid/RayTracedVolume_FP.glsl',
            '/data/glsl/valid/Render_VP.glsl',
            '/data/glsl/valid/sheldon-glslang-directives.frag.glsl'
        ]

        dir_path = os.path.dirname(os.path.realpath(__file__))
        for file in valid_files:

            file_path = common.file_on_disk(dir_path + file)
            invalid_glsl = glslang_validator.glslang_validator(file_path)
            self.assertFalse(invalid_glsl, 'Valid GLSL detected as invalid.')

            file_path = common.file_on_disk(dir_path + file)
            unsupported_version = glslang_validator.check_glsl_version(file_path)
            self.assertFalse(unsupported_version, 'Supported version detected as unsupported.')


if __name__ == '__main__':
    unittest.main()
