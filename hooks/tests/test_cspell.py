#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import unittest

import common
import cspell

# Be verbose by default
common.g_trace = True

# Set repo root
common.set_repo_root(os.getcwd())


class TestCSpell(unittest.TestCase):
    def test_check_cspell_with_invalid_cpp_file(self):
        # Load the test file
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file = common.file_on_disk(os.path.join(dir_path, 'data', 'check_cspell_invalid.cpp'))

        # Apply the hook
        result = cspell.cspell(file)
        self.assertTrue(result, "Invalid cpp file detected as valid.")

    def test_check_cspell_with_valid_cpp_file(self):
        # Load the test file
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file = common.file_on_disk(os.path.join(dir_path, 'data', 'check_cspell_valid.cpp'))

        # Apply the hook
        result = cspell.cspell(file)
        self.assertFalse(result, "Valid cpp file detected as invalid.")

    def test_check_cspell_with_ignored_file(self):
        # Load the test file
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file = common.file_on_disk(os.path.join(dir_path, 'data', 'check_cspell_invalid.ignored'))

        # Apply the hook
        result = cspell.cspell(file)
        self.assertFalse(result, "Ignored file detected as invalid.")

    def test_check_cspell_with_invalid_cpp_file_and_add_dict(self):
        # Load the test file
        dir_path = os.path.dirname(os.path.realpath(__file__))
        file = next(common.file_on_disk(os.path.join(dir_path, 'data', 'check_cspell_invalid.cpp')))
        result = cspell.check_file([file.path], add_dict_paths=os.path.join(dir_path, "data", ".cspell"))

        # Check result
        self.assertFalse(result, "Invalid cpp detected as valid, with additional dictionary.")


if __name__ == '__main__':
    unittest.main()
