#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import unittest

import check_xml_dependencies
import common

from xml.etree import ElementTree as ET


class TestCheckXmlDependencies(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(TestCheckXmlDependencies, self).__init__(*args, **kwargs)
        # Be verbose by default
        common.g_trace = True
        # Set the test data path.
        dir_path = os.path.dirname(os.path.realpath(__file__))
        self.repo_path = dir_path + '/data/dependency_test_repo/modules'

    def test_check_deps_with_missing_deps(self):
        # Load the test file
        file = common.file_on_disk(
            self.repo_path + '/missing_deps/rc/missing_deps.xml')

        # Apply the hook
        common.set_repo_root(self.repo_path)
        result = check_xml_dependencies.check_xml_dependencies(file)

        # Check result
        self.assertTrue(result, 'Missing dependencies not detected.')

    def test_missing_properties(self):
        # Load the test file
        file = common.file_on_disk(
            self.repo_path + '/missing_properties/rc/missing_properties.xml')

        # Apply the hook
        common.set_repo_root(self.repo_path)
        result = check_xml_dependencies.check_xml_dependencies(file)

        # Check result
        self.assertTrue(result, 'Missing Properties.cmake not detected.')

    def test_check_deps_with_valid_deps(self):
        # Load the test file
        file = common.file_on_disk(
            self.repo_path + '/valid_deps/rc/valid_deps.xml')

        # Apply the hook
        common.set_repo_root(self.repo_path)
        result = check_xml_dependencies.check_xml_dependencies(file)

        # Check result
        self.assertFalse(result, 'Valid requirements detected as incomplete.')

    def test_check_deps_with_same_module(self):
        # Load the test file
        file = common.file_on_disk(
            self.repo_path + '/same_module/rc/plugin.xml')

        # Apply the hook
        common.set_repo_root(self.repo_path)
        result = check_xml_dependencies.check_xml_dependencies(file)

        # Check result
        self.assertFalse(result, 'Valid requirements detected as incomplete.')

    def test_get_service_module(self):
        requirements = ['sight::module::io::vtk', 'sight::ui::qt', 'app::module::io', 'app::viz']

        service_config = ET.fromstring('<service type="sight::ui::qt::SEditor" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertTrue(result, 'Service module not found.')

        service_config = ET.fromstring('<service type="sight::ui::wx::SEditor" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertFalse(result, 'Service module should not be found.')

        service_config = ET.fromstring('<service type="app::viz::SRender" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertTrue(result, 'Service module not found.')

        # We assume a sub-target if app::viz would not exist
        service_config = ET.fromstring('<service type="app::viz::adaptor::SItem" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertTrue(result, 'Service module not found.')

        service_config = ET.fromstring('<service type="sight::module::io::vtk::SReader" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertTrue(result, 'Service module not found.')

        service_config = ET.fromstring('<service type="sight::module::io::SReader" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertFalse(result, 'Service module should not be found.')

        service_config = ET.fromstring('<service type="app::module::io::SReader" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertTrue(result, 'Service module not found.')

        # We assume a sub-target if app::module::io would not exist
        service_config = ET.fromstring('<service type="app::module::io::folder::SReader" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertTrue(result, 'Service module not found.')

        # Exceptions
        service_config = ET.fromstring('<service type="sight::viz::scene3d::SRender" />')
        result = check_xml_dependencies.get_service_module(service_config, requirements)
        self.assertTrue(result, 'Service module not found.')


if __name__ == '__main__':
    unittest.main()
