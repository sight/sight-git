#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import common
import re
import os.path
import itertools
import tempfile
from dataclasses import dataclass
from fnmatch import fnmatch
from multiprocessing import Pool
from itertools import repeat

GLSLANG_VALIDATOR_PATH = 'glslangValidator'

# cspell: ignore tesc, tese, DGLSL


@dataclass
class preprocessor_directives:
    defines: set  # Token found in #ifdef, #if defined(...), #elif defined().
    exclusive_defines: set  # Set of tuples with exclusive #define directives ignored when not exactly one is present.


def check_glslang_install():
    """Checks if the glslangValidator executable exists.

    Verifies that the path to executable has been set correctly.
    It can be set in the `.gitconfig` or in the system's `$PATH` (`%PATH%` on windows) variable.

    Returns:
        bool: True if the glslangValidator was found, False otherwise.

    """
    exec_error, msg = common.execute_command(GLSLANG_VALIDATOR_PATH + ' --version')
    failed = exec_error != 0

    if failed:
        common.error(msg)

    return failed


def get_shader_stage(glsl_filename):
    """Retrieves the shader stage from the filename.

    GLSL header files are considered as fragment programs.

    Args:
        glsl_filename (os.path): full or relative path to the glsl file.

    Returns:
        str: shader stage as a string recognized by glslangValidator's '-S' option.

    """
    tokens = glsl_filename.split('.')

    extension = ".".join(tokens[1:]).removesuffix('.glsl')

    glslang_files_extensions = {'vert', 'tesc', 'tese', 'geom', 'frag', 'comp'}

    # If the shader file has the stage in its name, just return it
    if(extension in glslang_files_extensions):
        return extension

    # Attempt to parse the name for legacy shaders, which follow the format name_XX.glsl
    name = tokens[0]

    stage_acronym = name.split('_')[-1]

    stage_map = {
        'VP': 'vert',
        'TC': 'tesc',
        'TE': 'tese',
        'GP': 'geom',
        'FP': 'frag',
        'CP': 'comp'
    }

    return stage_map.get(stage_acronym, 'frag')


def parse_all_pp_directives(glsl_file_content: str):
    """Returns every macro that can be defined for this particular GLSL file.

    Parses the file to find all define directives used by preprocessor commands.

    Args:
        glsl_file_content (str): the file's content

    Returns
        set: all the macro names that can be defined for this shader

    """
    # Regex matching the 'if', 'ifdef', 'ifndef' and 'elif' preprocessor command lines.
    pp_command_regex = r'^[\t ]*#[\t ]*(ifdef|elif|ifndef|if)[\t ]*(.*)$'
    # Regex matching macros in a preprocessor command.
    pp_macro_regex = r'(?:defined\s*\(\s*)?([a-zA-Z_][a-zA-Z0-9_]*)'
    # Regex to find comment at the end of a line.
    pp_comment_regex = r'(//|/\*)[^$]*'

    # Matches the // sheldon: glslang comments
    sheldon_glslang_regex = r'^(?://\s*sheldon\s*:\s*glslang\s*)(.*)'

    # Matches the require-exclusive() contents in the comments that matched sheldon_glslang_regex
    require_exclusive_regex = r'(?:\s*require-exclusive\()(.*?)(?:\))'

    # Analogue for discard() contents
    discard_define_regex = r'(?:\s*discard\()(.*?)(?:\))'

    result = preprocessor_directives(defines=set(), exclusive_defines=set())

    discarded_defines = set()

    # Extract everything following a #ifdef, #ifndef, #elif or #if preprocessor command
    for match in re.finditer(pp_command_regex, glsl_file_content, re.MULTILINE):
        # Remove comment if there is one.
        if match[2] != 'GLSL_LANG_VALIDATOR':
            no_comment = re.sub(pp_comment_regex, '', match[2])
            # Tokenize the result to find macro names.
            for define in re.finditer(pp_macro_regex, no_comment):
                result.defines.add(define[1])

    # Find // sheldon: glslang comments
    for match in re.finditer(sheldon_glslang_regex, glsl_file_content, re.MULTILINE):
        # Find require-exclusive() tokens
        for exclusive_defines in re.finditer(require_exclusive_regex, match[0], re.MULTILINE):
            # Get individual tokens to generate the combinations below
            tokens = str(exclusive_defines[1]).replace(" ", "").split(",")
            result.exclusive_defines.add(tuple(set(map(lambda d: '-D' + d + '=1', tokens))))
        # Find discard() tokens
        for ignored_defines in re.finditer(discard_define_regex, match[0], re.MULTILINE):
            tokens = str(ignored_defines[1]).replace(" ", "").split(",")
            discarded_defines = discarded_defines.union(set(tokens))

    # Remove them here to generate less combinations
    for define in discarded_defines:
        result.defines.discard(define)

    return result


def find_include_dirs(base_dir):
    """Find the potential include directories for this shader

    :param base_dir: directory where to start the search
    :return: list of include directories
    """
    include_dirs = []

    # Search for 'glsl' base dir in the path
    found = False
    glsl_dir = base_dir
    while True:
        base_dir, current_dir = os.path.split(glsl_dir)
        if current_dir == 'glsl':
            found = True
            break
        if base_dir == glsl_dir:
            break
        glsl_dir = base_dir

    if found:
        for r, dirs, files in os.walk(glsl_dir):
            for file in files:
                if re.search(r'\.inc\.glsl', file):
                    include = os.path.abspath(r)
                    if include not in include_dirs:
                        include_dirs += [include]

    return include_dirs


def all_pp_defines_combinations(glsl_file_content: str):
    """Compute every possible preprocessor define combination for the input file.

    Searches for all macros used by this files and yields definition combinations.
    The number of iterations is equal to the number of macros squared.

    Args:
        glsl_file_content (str): the content of the file.

    Yields:
        list[str]: a combination of preprocessor definition options recognized by glslangValidator.

    """
    pp_defines = parse_all_pp_directives(glsl_file_content)

    # Convert macro names to glslangValidator input parameters.
    command_defines = list(map(lambda d: '-D' + d + '=1', pp_defines.defines))

    for define in range(len(command_defines) + 1):
        for comb in itertools.combinations(command_defines, define):

            skip = False

            for exclusive_group in pp_defines.exclusive_defines:
                # Only keep combinations with exactly one
                if(len(set(comb).intersection(set(exclusive_group))) != 1):  # Also skip combinations with all missing
                    skip = True
                    break

            if(not skip):
                yield comb


def run_glslang_preproc(glsl_file_path, shader_stage, include_dirs, defines):
    """Runs glslangValidator to preprocess the input file.

    Args:
        glsl_file_path (os.path): path to the file to process
        shader_stage (str): shader stage in a format recognized by glslangValidator's '-S' option
        defines (list[str]): preprocessor definitions

    Returns:
        Optional[os.path]: the name to a temporary file with the preprocessed code or None if the preprocessor failed.

    """
    preprocessed_glsl_temp_file = tempfile.NamedTemporaryFile(delete=False)
    output_filename = preprocessed_glsl_temp_file.name

    cmd = GLSLANG_VALIDATOR_PATH + ' '  # glslangValidator executable.
    cmd += ' '.join(defines)  # Expand all macro definitions.
    cmd += ' -DGLSL_LANG_VALIDATOR '
    cmd += ' -E '  # Print preprocessed GLSL to the standard output.
    cmd += ' '.join(include_dirs)  # Expand all include directories.
    cmd += ' -S '  # Option to define the shader stage.
    cmd += shader_stage + ' '  # Shader stage.
    cmd += glsl_file_path  # Input file path.

    exec_error, out = common.execute_command(cmd,
                                             preprocessed_glsl_temp_file  # Output file.
                                             )

    # If it's an inclusion shader, we add a dummy version to fix the validator compiling.
    if fnmatch(glsl_file_path.lower(), '*.inc.glsl'):
        with open(output_filename, "w") as f:
            f.seek(0, 0)
            f.write("#version 450")

    if out is not None and out.decode() != '':
        print(out.decode())

    preprocessed_glsl_temp_file.close()

    fl = open(preprocessed_glsl_temp_file.name, "r+")
    output_log = fl.read()
    fl.close()

    if exec_error:
        common.error('glslang preprocessor failed for: ' + glsl_file_path)
        common.error('using the following #defines: ' + str(defines))
        common.error('Command: ' + str(cmd))
        common.error('Output log: ' + os.linesep + str(output_log))
        common.error('Aborting')
        output_filename = None

    return output_filename


def run_glslang_validator(glsl_file_path, shader_stage):
    """Runs glslangValidator to check if the input file is valid GLSL.

    Args:
        glsl_file_path (os.path): path to the file to process
        shader_stage (str): shader stage in a format recognized by glslangValidator's '-S' option

    Returns:
        bool: False if file compiles successfully, True otherwise.

    """
    exec_error, out = common.execute_command(GLSLANG_VALIDATOR_PATH  # glslangValidator executable.
                                             + ' -S '  # Option to define the shader stage.
                                             + shader_stage + ' '  # Shader stage.
                                             + glsl_file_path)  # Input file path.

    if out is not None and out.decode() != '':
        print(out.decode())

    if exec_error:
        common.error('glslangValidator failure on file: ' + glsl_file_path)
        common.error('Aborting')

    return exec_error


def validate_glsl_combination(pp_combination, file, shader_stage, include_dirs):
    """Checks if the input file is valid GLSL for a single combination.

    Args:
        pp_combination: preprocessor combination
        file (os.path): path to the file to process
        shader_stage (str): shader stage in a format recognized by glslangValidator's '-S' option
        include_dirs (os.path): includes directories where to search for headers

    Returns:
        bool: False if file compiles successfully, True otherwise.

    """
    common.note('Checking file ' + file.path + ' with: ' + str(pp_combination))
    tmp_file = None
    try:
        # run the preprocessor separately first because glslangValidator
        # can not do both at the same time ...
        tmp_file = run_glslang_preproc(file.path, shader_stage, include_dirs, pp_combination)
        abort = tmp_file is None

        if not abort:
            # validate the preprocessed glsl code
            abort = run_glslang_validator(tmp_file, shader_stage)

    finally:
        # delete the temporary file even if an exception is encountered.
        if tmp_file is not None and os.path.exists(tmp_file):
            os.remove(tmp_file)

    if abort:
        raise RuntimeError

    return abort


def validate_glsl(file, sight_include_dirs):
    """Checks if the input file is valid GLSL for every possible.

    Compiles the file with every preprocessor define combination.

    Args:
        file (common.FileAtIndex): shader source code to compile
        sight_include_dirs (os.path): extra directories where to search for includes

    Returns:
        bool: False if the shader compiles with any macro combination, True otherwise.

    """
    file_contents = file.contents.decode()
    filename = os.path.basename(file.path)
    shader_stage = get_shader_stage(filename)
    include_dirs = find_include_dirs(os.path.dirname(file.path)) + sight_include_dirs
    include_dirs_args = list(map(lambda i: '-I' + i, include_dirs))

    # Compile the shader with every possible macro combination.
    with Pool() as pool:
        future = pool.starmap_async(validate_glsl_combination,
                                    zip(all_pp_defines_combinations(file_contents),
                                        repeat(file), repeat(shader_stage), repeat(include_dirs_args)))
        try:
            future.get()
        except RuntimeError:
            return True

    return False


def glslang_validator(files):
    """Checks if all input GLSL files are valid.

    Args:
        files (list[common.FileAtIndex]): files to process

    Returns:
        bool: False if the files compiles with any macro combination, True otherwise.

    """
    abort = False

    global GLSLANG_VALIDATOR_PATH

    if common.g_glslang_validator_path is not None and len(common.g_glslang_validator_path) > 0:
        GLSLANG_VALIDATOR_PATH = common.g_glslang_validator_path
    else:
        GLSLANG_VALIDATOR_PATH = common.get_option(
            'glslang-validator-hook.glslang-validator-path', default=GLSLANG_VALIDATOR_PATH, type='--path').strip()

    glsl_files = [f for f in files if fnmatch(f.path.lower(), '*.glsl')]

    # Don't run glslangValidator if there are no glsl files to check.
    if not glsl_files:
        return False

    if check_glslang_install():
        common.error('Failed to launch glslangValidator.=')
        return True

    repo_name = common.get_repo_name()
    sight_include_dirs = []
    if repo_name != 'sight':
        default_path = os.path.join(os.getcwd(), '../sight/libs/viz/scene3d/rc/Media/glsl/')
        sight_glsl_shader_path = common.get_option('sight.glsl-shader-path', default=default_path)

        sight_include_dirs = find_include_dirs(os.path.dirname(sight_glsl_shader_path))

    for f in glsl_files:
        abort = validate_glsl(f, sight_include_dirs) or abort

    return abort


def check_glsl_version(files):
    """Checks if the glsl version is supported by sight.

    Supported versions are 110, 120, 130, 140, 150, 330, 400 and 410.

    Args:
        files (list[common.FileAtIndex]): files to process

    Returns:
        bool: False if the files compiles with any macro combination, True otherwise.

    """
    abort = False
    glsl_version_regex = r'^[\t ]*#[\t ]*version[\t ]+([1-9][0-9]{2})'

    # Allowed versions.
    allowed_versions = ['110', '120', '130', '140', '150', '330', '400', '410', '420', '430', '440', '450']

    for f in files:
        # `inc` files are skipped since they must not contain any version.
        if fnmatch(f.path.lower(), '*.glsl') and not fnmatch(f.path.lower(), '*.inc.glsl'):
            common.note('Checking glsl version in : ' + f.path)
            file_contents = f.contents.decode()
            match = re.search(glsl_version_regex, file_contents, re.MULTILINE)

            if not match:
                common.error('No version defined in: ' + f.path)
                abort = True
            else:
                version = match[1]

                if version not in allowed_versions:
                    sep = ' '
                    common.error('Unsupported glsl version: ' + version)
                    common.error('Supported versions are: ' + sep.join(allowed_versions))
                    abort = True

    return abort


hooks = {
    'glslang_validator': glslang_validator,
    'check_glsl_version': check_glsl_version
}
