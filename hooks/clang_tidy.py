"""
Clang-tidy your code.

.gitconfig configuration:

[sight-hooks]
    hooks = clang-tidy

[clang-tidy-hook]
    source-patterns = *.cpp *.cxx *.c
    header-patterns = *.hpp *.hxx *.h
    generate-compilation-db = when_absent
    build-path = build/debug
    report-modes = console,log,html,codequality
"""

import os
import re
import subprocess
from fnmatch import fnmatch
from enum import Enum, auto

import common


class CompDbGen(Enum):
    ALWAYS = auto(),
    WHEN_ABSENT = auto(),
    NEVER = auto()

    @classmethod
    def new(cls, generate_compilation_db):
        if generate_compilation_db == 'always':
            return CompDbGen.ALWAYS
        elif generate_compilation_db == 'when_absent':
            return CompDbGen.WHEN_ABSENT
        elif generate_compilation_db == 'never':
            return CompDbGen.NEVER
        else:
            raise Exception('Invalid value for "clang-tidy-hook.generate-compilation-db": "%s". '
                  'Valid values are "always", "when_absent" and "never".' % generate_compilation_db)


class ReportModes:
    def __init__(self, report_modes):
        self.console = self.log = self.html = self.codequality = False
        for report_mode in report_modes:
            if report_mode == 'console':
                self.console = True
            elif report_mode == 'html':
                self.html = True
            elif report_mode == 'log':
                self.log = True
            elif report_mode == 'codequality':
                self.codequality = True
            else:
                common.warn('Unknown report mode: "%s". Ignored.' % report_mode)
        if not (self.console or self.html or self.log or self.codequality):
            common.warn('No report mode are enabled. "console" is enabled.')
            self.console = True
        self.log = self.log or self.html


def check_run_clang_tidy():
    p = subprocess.Popen(['run-clang-tidy', '--help'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    p.communicate()
    if p.wait() == 0:
        return 'run-clang-tidy'
    raise


def check_aha():
    try:
        p = subprocess.Popen(['aha', '--version'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        p.communicate()
        if p.wait() != 0:
            return False
    except FileNotFoundError:
        return False
    return True


def clang_tidy(files, checks=None):
    source_patterns = common.get_option('clang-tidy-hook.source-patterns', default='*.cpp *.cxx *.c').split()
    header_patterns = common.get_option('clang-tidy-hook.header-patterns', default='*.hpp *.hxx *.h').split()
    code_patterns = source_patterns + header_patterns

    repo_root = common.get_repo_root()
    files_to_analyze = []
    for f in files:
        if any(fnmatch(f.path.lower(), p) for p in code_patterns):
            files_to_analyze.append(os.path.join(repo_root, f.path))
    if len(files_to_analyze) == 0:
        return False

    generate_compilation_db = common.get_option('clang-tidy-hook.generate-compilation-db', default='never')
    build_path = common.get_option('clang-tidy-hook.build-path', default=None)
    try:
        comp_db_gen = CompDbGen.new(generate_compilation_db)
    except Exception as err:
        common.error(str(err))
        return True

    compile_cmd_path = os.path.join(build_path if build_path is not None else repo_root, 'compile_commands.json')
    compile_cmd_exists = os.path.exists(compile_cmd_path)
    if comp_db_gen == CompDbGen.ALWAYS or (comp_db_gen == CompDbGen.WHEN_ABSENT and not compile_cmd_exists):
        if build_path is None:
            explanation = '"always"' if comp_db_gen == CompDbGen.ALWAYS \
                          else '"when_absent" and the compilation database is absent'
            common.error('The compilation database generation is set to %s, but no build_path is provided. '
                         'Please set "clang-tidy-hook.build_path" to the appropriate directory.' % explanation)
            return True
        p = subprocess.Popen(['cmake', '-S' + repo_root, '-B' + build_path,
                             '-DCMAKE_EXPORT_COMPILE_COMMANDS=ON', '-DCMAKE_BUILD_TYPE=Debug',
                              '-DSIGHT_ENABLE_CLANG_TIDY=ON', '-DSIGHT_ENABLE_PCH=OFF'],
                             stdout=subprocess.DEVNULL)

        if p.wait() != 0:
            common.error('CMake returned with error %d while generating the compilation database' % p.returncode)
            return True
        p = subprocess.Popen(['sed', '-i', 's/-gmodules//g', os.path.join(build_path, 'compile_commands.json')])
        if p.wait() != 0:
            common.warn('sed returned with error %d while modifying the compilation database' % p.returncode)
    elif comp_db_gen == CompDbGen.NEVER and not compile_cmd_exists:
        common.error('%s doesn\'t exist. Please generate it by adding CMAKE_EXPORT_COMPILE_COMMANDS=ON to your CMake '
                     'configuration, re-run and move the resulting compile_commands.json in the repository root, or, '
                     'using git config, set "clang-tidy-hook.generate-compilation-db" to "when_absent" or "always" and '
                     'set "clang-tidy-hook.build-path" to your build directory.' % compile_cmd_path)
        return True

    run_clang_tidy = check_run_clang_tidy()

    maybe_checks = ['-checks=' + checks] if checks is not None else []
    maybe_fix = ['-fix'] if common.g_enable_reformat else []
    maybe_build_path = ['-p' + build_path] if build_path is not None else []

    p = subprocess.Popen(
        [run_clang_tidy, '-j%d' % os.cpu_count(), *maybe_build_path, *maybe_fix, *maybe_checks, *files_to_analyze],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE)
    out, err = p.communicate()

    report_modes_opt = common.get_option('clang-tidy-hook.report-modes', default='console').split(',')
    report_modes = ReportModes(report_modes_opt)
    if report_modes.html:
        if not check_aha():
            common.warn('HTML report is enabled but aha can\'t be found. HTML report is disabled.')
            report_modes.html = False
    if report_modes.log:
        log_file = open(os.path.join(repo_root, 'clang-tidy.log'), 'w')
    if report_modes.codequality:
        cq_file = open(os.path.join(repo_root, 'clang-tidy.json'), 'w')
        cq_file.write('[')
        first_cq = True
    for line in out.splitlines():
        decoded_line = line.decode()
        if report_modes.console:
            print(decoded_line)
        if report_modes.log:
            log_file.write(decoded_line + '\n')
        if report_modes.codequality:
            words = re.findall('.*%s/([^:]*):([^:]*):[^:]*: .*(error|warning):[^ ]* .*\\[1m(.*) (\\[.*\\]).*'
                               % repo_root, decoded_line)
            if(words):
                filename, num_line, err_warn, desc, check_name = words[0]
                severity = 'major' if err_warn == 'error' else 'minor'
                cq_file.write('{5}{{"description": "{2} {3}", "fingerprint": "{0}:{1}{3}", "severity": "{4}", '
                              '"location": {{"path": "{0}", "lines": {{"begin": {1} }} }} }}'
                              .format(filename, num_line, desc, check_name, severity, '' if first_cq else ','))
                first_cq = False
    if report_modes.log:
        log_file.close()
    if report_modes.codequality:
        cq_file.write(']')
        cq_file.close()
    if report_modes.html:
        with open(os.path.join(repo_root, 'clang-tidy.html'), 'w') as html_file:
            aha = subprocess.Popen(['aha', '-f', os.path.join(repo_root, 'clang-tidy.log')], stdout=html_file)
            if aha.wait() != 0:
                common.warn('aha returned with error %d while generating the HTML report' % aha.returncode)

    ok = p.wait() == 0
    if not ok:
        common.error('There was errors while running clang-tidy:')
        print(err.decode())
    return not ok


def clang_tidy_fix(files):
    clang_tidy(files,
               '-*,cppcoreguidelines-init-variables,cppcoreguidelines-prefer-member-initializer,'
               'cppcoreguidelines-pro-type-member-init,hicpp-deprecated-headers,hicpp-member-init,'
               'hicpp-named-parameter,hicpp-noexcept-move,hicpp-use-auto,hicpp-use-emplace,hicpp-use-equals-default,'
               'hicpp-use-equals-delete,hicpp-use-nullptr,hicpp-use-override,llvm-else-after-return,'
               'llvm-namespace-comment,misc-unused-alias-decls,modernize-avoid-bind,modernize-concat-nested-namespaces,'
               'modernize-deprecated-headers,modernize-loop-convert,modernize-make-shared,modernize-pass-by-value,'
               'modernize-return-braced-init-list,modernize-use-auto,modernize-use-default-member-init,'
               'modernize-use-emplace,modernize-use-equals-default,modernize-use-equals-delete,modernize-use-nodiscard,'
               'modernize-use-nullptr,modernize-use-override,modernize-use-transparent-functors,'
               'performance-faster-string-find,performance-for-range-copy,performance-inefficient-vector-operation,'
               'performance-noexcept-move-constructor,performance-trivially-destructible,'
               'performance-unnecessary-value-param,readability-avoid-const-params-in-decls,'
               'readability-const-return-type,readability-convert-member-functions-to-static,'
               'readability-delete-null-pointer,readability-else-after-return,readability-implicit-bool-conversion,'
               'readability-inconsistent-declaration-parameter-name,readability-isolate-declaration,'
               'readability-make-member-function-const,readability-named-parameter,readability-qualified-auto,'
               'readability-redundant-smartptr-get,readability-redundant-string-cstr,readability-redundant-string-init,'
               'readability-simplify-boolean-expr,readability-static-accessed-through-instance,'
               'readability-uppercase-literal-suffix')


hooks = {
    'clang-tidy': clang_tidy,
    'clang-tidy-fix': clang_tidy_fix
}
