#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Make sure you respect the minimal coding rules and gently reformat files for you.

.gitconfig configuration :

[fw4spl-hooks]
    hooks = codingstyle

[codingstyle-hook]
    source-patterns = *.cpp *.cxx *.c
    header-patterns = *.hpp *.hxx *.h
    misc-patterns = *.cmake *.txt *.xml *.json
    uncrustify-path=C:/Program files/uncrustify/uncrustify.exe
    additional-projects = "D:/Dev/src/fw4spl-ar;D:/Dev/src/fw4spl-ext"

Available options are :
source-patterns : file patterns to process as source code files - default to *.cpp *.cxx *.c
header-patterns : file patterns to process as header files - default to *.hpp *.hxx *.h
misc-patterns : file patterns to process as non-source code files (build, configuration, etc...)
                Reformatting is limited to TABs and EOL - default to *.options *.cmake *.txt *.xml
uncrustify-path : path to the uncrustify program - default to uncrustify
additional-projects : additional fw4spl repositories paths used to sort includes (separated with a ;).
                      default parent folder of the current repository.

"""

import codecs
import os
import re
from fnmatch import fnmatch

import common
import sortincludes
from common import FormatReturn

SEPARATOR = '%s\n' % ('-' * 79)


def FILEWARN(x):
    return '  - %s' % os.path.relpath(x, common.get_repo_root())


UNCRUSTIFY_PATH = 'uncrustify'
BACKUP_LIST_FILE = 'backupList'
YEAR = None

LICENSE_BASE = r'/\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*' \
               r'\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\n' \
               r' \*\n' \
               r' \* (Copyright[\d\D]*)*' \
               r' \*\n' \
               r' \* This file is part of ([\w-]*).\n' \
               r' \*\n' \
               r' \* ([\w-]*) is free software: you can redistribute it and/or modify it under\n' \
               r' \* the terms of the GNU Lesser General Public License as published by\n' \
               r' \* the Free Software Foundation, either version 3 of the License, or\n' \
               r' \* \(at your option\) any later version.\n' \
               r' \*\n' \
               r' \* ([\w-]*) is distributed in the hope that it will be useful,\n' \
               r' \* but WITHOUT ANY WARRANTY; without even the implied warranty of\n' \
               r' \* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n' \
               r' \* GNU Lesser General Public License for more details.\n' \
               r' \*\n' \
               r' \* You should have received a copy of the GNU Lesser General Public\n' \
               r' \* License along with Sight. If not, see <https://www.gnu.org/licenses/>.\n' \
               r' \*\n'

LICENSE_LGPL = LICENSE_BASE + \
    r' (\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*' \
    r'\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*/\n)|' \
    r' (\* This file incorporates work covered by the following copyright and\n' \
    r' \* permission notice:)'

LICENSE_REPLACE = LICENSE_BASE + \
    r' \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*' \
    r'\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*/\n'

LICENSE_NONFREE = r'/\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*' \
                  r'\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\n' \
                  r' \* (Copyright[\d\D]*)*' \
                  r' \*\n' \
                  r' \* This file is part of ([\w-]*).\n' \
                  r' \*\n' \
                  r' \* ([\w-]*) can not be copied, modified and/or distributed without\n' \
                  r' \* the express permission of ([\w ]*).\n' \
                  r' \*\n' \
                  r' \*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*' \
                  r'\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*\*/'

# This strings will be used as Python strings, so we must convert them from raw strings
LICENSE_REPLACE = codecs.decode(LICENSE_REPLACE, 'unicode_escape')
LICENSE_REPLACE_NONFREE = codecs.decode(LICENSE_NONFREE, 'unicode_escape')

# On Windows, we need to be more precise about the executable to be found
# Otherwise false positives will occur
if os.name == 'nt':
    NPM_BIN = 'npm.cmd'
    PRETTIER_BIN = 'prettier.cmd'
else:
    NPM_BIN = 'npm'
    PRETTIER_BIN = 'prettier'

PRETTIER_XML = '@prettier/plugin-xml'


def coding_style(files, enable_reformat, check_commits_date):
    """
    Check format a list of files and optionally reformat them
    :param files: list of file paths
    :param enable_reformat: True or False
    :param check_commits_date: if True, use the reference date from the commit instead of the current system time
    :return:[True|false, file1, file2, file3, ...]: Whether files were reformatted and the list of files reformatted
    """
    source_patterns = common.get_option('codingstyle-hook.source-patterns', default='*.cpp *.cxx *.c *.cu').split()
    header_patterns = common.get_option('codingstyle-hook.header-patterns', default='*.hpp *.hxx *.h *.cuh').split()
    cmake_patterns = common.get_option('codingstyle-hook.cmake-patterns', default='*CMakeLists*.txt *.cmake').split()
    prettier_patterns = common.get_option('codingstyle-hook.prettier-patterns', default='*.xml *.json *.yml').split()

    code_patterns = source_patterns + header_patterns
    include_patterns = code_patterns + cmake_patterns + prettier_patterns

    sort_includes = common.get_option('codingstyle-hook.sort-includes', default="true", type='--bool') == "true"

    repo_root = common.get_repo_root()

    if repo_root is None:
        common.warn("Cannot find 'sight' repository structure")

    global UNCRUSTIFY_PATH

    if common.g_uncrustify_path_arg is not None and len(common.g_uncrustify_path_arg) > 0:
        UNCRUSTIFY_PATH = common.g_uncrustify_path_arg
    else:
        UNCRUSTIFY_PATH = common.get_option('codingstyle-hook.uncrustify-path', default=UNCRUSTIFY_PATH,
                                            type='--path').strip()

    common.note('Using uncrustify: ' + UNCRUSTIFY_PATH)

    if common.execute_command(UNCRUSTIFY_PATH + ' -v -q').status != 0:
        common.error('Failed to launch uncrustify.\n')
        return []

    if common.execute_command('cmake-format -v').status != 0:
        common.error('Failed to launch cmake-format.\n')
        return []

    if common.execute_command(PRETTIER_BIN + ' -v').status != 0:
        common.error('Failed to launch prettier. Check documentation at https://github.com/prettier/plugin-xml.\n')
        return []

    # Find prettier xml plugin
    check_result = common.execute_command(NPM_BIN + ' config get prefix')
    if check_result.status != 0:
        common.error('Failed to get npm global root.\n')
        return []

    global PRETTIER_XML
    PRETTIER_XML = check_result.out.decode("utf-8").strip()

    if os.name != 'nt':
        PRETTIER_XML += '/lib'

    PRETTIER_XML += '/node_modules/@prettier/plugin-xml/src/plugin.js'

    checked = set()

    reformatted_list = []
    sortincludes.find_libraries_and_modules(repo_root)

    ret = False
    count = 0
    reformat_count = 0
    for f in files:
        if f in checked or not any(f.fnmatch(p) for p in include_patterns):
            continue

        content = f.contents
        if not common.binary(content):

            # Do this last because contents of the file will be modified by uncrustify
            # Thus the variable content will no longer reflect the real content of the file
            file_path = os.path.join(repo_root, f.path)
            if os.path.isfile(file_path):
                res = check_and_format_file(file_path, enable_reformat, code_patterns, header_patterns, cmake_patterns,
                                            prettier_patterns, sort_includes, f.status, check_commits_date)

                count += 1

                if res.modified:
                    reformatted_list.append(f.path)
                    reformat_count += 1

                if res.erroneous:
                    # Error in reformatting
                    ret = True

        checked.add(f)

    common.note('%d file(s) checked, %d file(s) reformatted.' % (count, reformat_count))

    return ret, reformatted_list


def check_and_format_file(source_file, enable_reformat, code_patterns, header_patterns, cmake_patterns,
                          prettier_patterns, sort_includes, status, check_commits_date):
    """
    Reformat a single file according to minimal coding-style rules
    :param source_file: path to the file
    :param enable_reformat: True or False
    :param code_patterns: List of file extensions considered as C++ code, i.e. [*.cpp, *.c, *.hpp, *.h]
    :param header_patterns: List of file extensions considered as C++ header files code, i.e. [*.hpp, *.h]
    :param cmake_patterns: List of file extensions considered as cmake files, i.e. [*CMakeLists*.txt, *.cmake]
    :param prettier_patterns: List of file extensions supported by prettier [*.xml, *.json, *.yml]
    :param sort_includes: Should we check and reformat includes order ?
    :param status: git status of the file : 'A' if this is a new file, 'M' for a modification
    :param check_commits_date: if True, use the reference date from the commit instead of the current system time
    :return: FormatReturn object indicating a success, a failure, or a reformat
    """
    # Invoke uncrustify for source code files
    if any(fnmatch(source_file, p) for p in code_patterns):

        common.trace('Launching uncrustify on : ' + source_file)
        config_file = os.path.join(os.path.dirname(__file__), 'uncrustify.cfg')

        ret = check_and_fix_license(source_file, enable_reformat, status, check_commits_date)

        # Sort headers
        if sort_includes is True:
            ret.add(sortincludes.sort_includes(source_file, enable_reformat))

        # Fix header guard (we skip PCH because they are compiled once and #pragma once raises a warning in this case)
        if any(fnmatch(source_file, p) and not fnmatch(source_file, "*/pch.hpp") for p in header_patterns):
            ret.add(fix_header_guard(source_file, enable_reformat))

        # We need to specify the language to Uncrustify for CUDA files
        [_, ext] = os.path.splitext(source_file)
        cpp_arg = ' -l CPP' if ext != ".c" and ext != ".h" else ''

        # Uncrustify command
        command = UNCRUSTIFY_PATH + ' -c ' + config_file + ' -q %s ' + source_file + cpp_arg

        if enable_reformat is True:
            # Check first
            uncrustify = common.execute_command(command % '--check')

            if uncrustify.status != 0:
                uncrustify = common.execute_command(command % '--replace --no-backup --if-changed')
                if uncrustify.status != 0:
                    common.error('Uncrustify failure on file: ' + source_file)
                    common.error(uncrustify.out)
                    ret.erroneous = True
                else:
                    ret.modified = True
        else:
            uncrustify = common.execute_command(command % '--check')

            if uncrustify.status != 0:
                common.error('Uncrustify failure on file: ' + source_file)
                ret.erroneous = True

        return ret

    # Invoke cmake-format and cmake-lint
    elif any(fnmatch(source_file, p) for p in cmake_patterns):
        ret = FormatReturn()

        # Build cmake-format base command
        config_file = os.path.join(os.path.dirname(__file__), 'cmake-format.cfg')
        base_command = 'cmake-format --config-files=' + config_file + ' '

        # Execute cmake-format check command
        check_command = base_command + '--check ' + source_file
        common.trace('Executing ' + check_command)
        check_result = common.execute_command(check_command)

        if check_result.status != 0:
            if enable_reformat is True:
                format_command = base_command + '--in-place ' + source_file
                common.trace('Executing ' + format_command)
                format_result = common.execute_command(format_command)

                if format_result.status != 0:
                    common.error('cmake-format format failure on file: ' + source_file)
                    common.error(format_result.out)
                    ret.erroneous = True
                else:
                    ret.modified = True
            else:
                common.error('cmake-format check failure on file: ' + source_file)
                common.error(check_result.out)
                ret.erroneous = True

        # Build cmake-lint base command
        lint_command = 'cmake-lint --config-files=' + config_file + ' --suppress-decorations ' + source_file
        common.trace('Executing ' + lint_command)
        lint_result = common.execute_command(lint_command)

        if lint_result.status != 0:
            common.error('cmake-lint check failure on file: ' + source_file)
            common.error(lint_result.out)
            ret.erroneous = True

        return ret

    elif any(fnmatch(source_file, p) for p in prettier_patterns):
        ret = FormatReturn()

        # Build html tidy base command
        base_command = (
            PRETTIER_BIN
            + ' --plugin=' + PRETTIER_XML
            + ' --print-width=240'
            + ' --tab-width=4'
            + ' --xml-quote-attributes=double'
            + ' --xml-whitespace-sensitivity=preserve'
        )

        # Execute html prettier check command
        check_command = base_command + ' --check ' + source_file
        common.trace('Executing ' + check_command)
        check_result = common.execute_command(check_command)

        if check_result.status != 0:
            if enable_reformat is True:
                format_command = base_command + ' --write ' + source_file
                common.trace('Executing ' + format_command)
                format_result = common.execute_command(format_command)

                if format_result.status != 0:
                    common.error('Prettier format failure on file: ' + source_file)
                    common.error(format_result.out)
                    ret.erroneous = True
                else:
                    ret.modified = True
            else:
                common.error('Prettier check failure on file: ' + source_file)
                common.error(check_result.out)
                ret.erroneous = True

        return ret


def check_and_fix_license_year(_content, _path, _enable_reformat, _status, _year, _entities):
    """
    Fix the license year for the copyrights holders
    :param _content: content of the file
    :param _path: file path
    :param _enable_reformat: if True, the function will try to update the date
    :param _status: git status of the file : 'A' if this is a new file, 'M' for a modification
    :param _year: actual year that should be checked for
    :param _entities: list of entities mentioned in the copyrights
    :return: FormatReturn object indicating a success, a failure, or a reformat
    """
    modified_content = _content

    year_ok = False
    for e in _entities:
        # Only check year for existing copyright lines
        copyright_regex = r'.*Copyright \(C\) (?:[0-9]*-)?([0-9]*) ' + e
        match = re.search(copyright_regex, _content)
        if match:
            year = match.group(1)
            if year == str(_year):
                year_ok = True
                break

    if not year_ok:

        if _enable_reformat:
            e = common.get_git_author_entity()

            copyright_regex = r'(.*Copyright \(C\) )([0-9]*-)?([0-9]*) ' + e
            match = re.search(copyright_regex, _content)
            if match.group(2):
                modified_content = re.sub(copyright_regex, r'\1\g<2>' + str(_year) + ' ' + e, _content)
            else:
                modified_content = re.sub(copyright_regex, r'\1\g<3>-' + str(_year) + ' ' + e, _content)

            common.note('Licence year fixed in : ' + FILEWARN(_path))
            with open(_path, 'wb') as source_file:
                source_file.write(modified_content.encode())
            return FormatReturn(modified=True)

        else:
            common.error('Licence year in : ' + FILEWARN(_path) + ' is not up-to-date.')
            return FormatReturn(erroneous=True)

    return FormatReturn()


# ------------------------------------------------------------------------------

def check_and_fix_license(_path, _enable_reformat, _status, _check_commits_date):
    """
    Check and optionally fix that the LGPL licence header is present and with the correct year
    :param _path: file path
    :param _enable_reformat: if True, the function will try to update the date
    :param _status: git status of the file : 'A' if this is a new file, 'M' for a modification
    :param _check_commits_date: if True, use the reference date from the commit instead of the current system time
    :return: FormatReturn object indicating a success, a failure, or a reformat
    """
    with open(_path, 'r', encoding='utf-8') as source_file:
        content = source_file.read()

    common.trace('Checking for LGPL license in: ' + _path)

    # YEAR can be hacked outside by unit-testing
    global YEAR
    if YEAR is None:
        YEAR = common.get_file_datetime(_path, _check_commits_date).year

    license_type = common.get_license(_path)
    assert (len(license_type) >= 3)
    assert (isinstance(license_type[0], bool))

    if license_type[0]:
        license_text = LICENSE_LGPL
    else:
        license_text = LICENSE_NONFREE

    # Look for the license pattern
    licence_number = len(re.findall(license_text, content, re.MULTILINE))

    entities = license_type[2:]

    if licence_number > 1:

        common.error("There should be just one licence header per file in :" + FILEWARN(_path) + ".")
        return FormatReturn(erroneous=True)

    elif licence_number < 1:

        if _enable_reformat:

            # No license present, we add it
            if license_type[0]:
                lic = LICENSE_REPLACE
            else:
                lic = LICENSE_REPLACE_NONFREE

            # Insert copyrights
            copyrights = ''
            e = common.get_git_author_entity()
            if e == 'IRCAD France':
                entities = [e]

            for e in entities:
                copyrights += (r'\* Copyright \(C\) %s ' + e) % YEAR
                if not license_type[0]:
                    copyrights += ' - All rights reserved.'
                copyrights += '\n'

            lic = lic.replace(r' \* (Copyright[\d\D]*)*', copyrights)

            # Insert software name
            lic = lic.replace(r'([\w-]*)', license_type[1])

            # Insert permissions if present
            perms = ''
            for e in entities:
                perms += e + ' and '
            lic = lic.replace(r'([\w ]*)', perms[:-5])

            lic = lic.replace('\\', '')

            with open(_path, 'wb') as source_file:

                preamble = lic + '\n\n'
                source_file.write(preamble.encode())
                source_file.write(content.encode())

            common.note('License header added in : ' + FILEWARN(_path) + '.')
            return FormatReturn(modified=True)

        else:

            common.error("There should be at least one licence header per file in :" + FILEWARN(_path) + ".")
            return FormatReturn(erroneous=True)

    # Everything looks fine, just check if the license year is correct now
    return check_and_fix_license_year(content, _path, _enable_reformat, _status, YEAR, entities)


def fix_header_guard(path, enable_reformat):
    """
    Check the header guard consistency
    :param path: path to the header file
    :param enable_reformat: if True, the function will fix the header guard
    :return: FormatReturn object indicating a success, a failure, or a reformat
    """
    with open(path, 'r', encoding='utf-8') as source_file:
        content = source_file.read()

    # Regex for '#pragma once'
    single_comment = r"(\/\/([^(\n|\r)]|\(|\))*)"
    multi_comment = r"(\/\*([^\*\/]|\*[^\/]|\/)*\*\/)"
    useless_char = "\t| |\r"
    pragma_once = "#pragma(" + useless_char + ")+once"
    all_before_pragma = ".*" + pragma_once + "(" + useless_char + ")*\n"

    # Number of occurrences of '#pragma once'
    pragma_number = len(re.findall(pragma_once, content, re.MULTILINE))
    if pragma_number > 1:

        common.error("There should be just one '#pragma once' per file in :" + FILEWARN(path) + ".")
        return FormatReturn(erroneous=True)

    elif pragma_number < 1:

        # Add 'pragma once'
        if enable_reformat:

            match = re.search("(" + single_comment + "|" + multi_comment + "|" + useless_char + "|\n)*", content,
                              re.MULTILINE)

            with open(path, 'wb') as source_file:
                source_file.write(match.group(0).encode())
                source_file.write(b"#pragma once\n\n")
                source_file.write(content.replace(match.group(0), "").encode())

            common.note("'#pragma once' fixed in :" + FILEWARN(path))
            return FormatReturn(modified=True)

        else:

            common.error("There should be at least one '#pragma once' per file in :" + FILEWARN(path) + ".")
            return FormatReturn(erroneous=True)

    # Here, it has only one occurrence that must be checked

    # Get all string before first '#pragma once'
    out = re.search(all_before_pragma, content, re.DOTALL).group(0)

    # Remove '#pragma once'
    match2 = re.search(pragma_once, out, re.DOTALL)
    out = out.replace(match2.group(0), "")

    # Remove multi line comments
    while len(re.findall(multi_comment, out, re.DOTALL)) != 0:
        match2 = re.search(multi_comment, out, re.DOTALL)
        out = out.replace(match2.group(0), "")

    # Remove single line comments
    while len(re.findall(single_comment, out, re.DOTALL)) != 0:
        match2 = re.search(single_comment, out, re.DOTALL)
        out = out.replace(match2.group(0), "")

    # If it's not empty, they are an error
    if len(re.findall("[^\n]", out, re.DOTALL)) != 0:
        common.error(
            ("Unexpected : '%s' before #pragma once in :" % re.search("^.+$", out, re.MULTILINE).group(0)) + FILEWARN(
                path) + ".")
        return FormatReturn(erroneous=True)

    # Check space number between '#pragma' and 'once'
    if len(re.findall("#pragma once", content, re.DOTALL)) == 0:

        if enable_reformat:
            # Get all string before first '#pragma once'
            out = re.search(all_before_pragma, content, re.DOTALL).group(0)

            # Remove '#pragma once'
            match2 = re.search(pragma_once, out, re.DOTALL)
            out2 = out.replace(match2.group(0), "")

            with open(path, 'wb') as source_file:

                source_file.write(out2.encode())
                source_file.write(b"#pragma once\n")
                source_file.write(content.replace(out, "").encode())

            return FormatReturn(modified=True)

        else:

            common.error("Needed : '#pragma once', actual : '" + re.search(pragma_once, content, re.DOTALL).group(
                0) + "' in file :" + FILEWARN(path) + ".")
            return FormatReturn(erroneous=True)

    return FormatReturn()
