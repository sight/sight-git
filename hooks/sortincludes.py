#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import re

import common
from common import FormatReturn

g_libs = []
g_modules = []


def find_current_library(path):
    repo_root = common.get_repo_root()
    if os.path.commonpath([repo_root, path]) != repo_root:
        raise RuntimeError('Wrong call, the requested file is not located inside the project repository.')

    cur_path = path
    found = False
    while cur_path != repo_root:
        if os.path.exists(os.path.join(cur_path, 'CMakeLists.txt')):
            cur_path = os.path.join(cur_path, 'CMakeLists.txt')
            found = True
            break
        cur_path = os.path.normpath(os.path.join(cur_path, '..'))

    if not found:
        raise RuntimeError('lib path not found')

    # Now we take the second last element (last element is 'CMakeLists.txt')
    lib_name = cur_path.split(os.path.sep)[-2]

    return lib_name


def find_libraries_and_modules(project_dir):
    global g_libs
    global g_modules

    g_libs = []
    g_modules = []

    if not os.path.isdir(project_dir):
        common.warn("%s isn't a valid directory." % project_dir)
    list_src_dirs = list(map(lambda x: os.path.join(project_dir, x), ['apps', 'libs', 'modules']))
    for src_dir in list_src_dirs:
        for root, dirs, files in os.walk(src_dir):
            rootdir = os.path.split(root)[1]
            # Do not inspect hidden folders
            if not rootdir.startswith("."):
                for file in files:
                    if file == "CMakeLists.txt":
                        with open(os.path.join(root, file)) as f:
                            m = re.search(r'sight_add_target *\( *\n? *(\w*)', f.read(), re.MULTILINE)
                            if m:
                                rel_root = os.path.relpath(root, project_dir)
                                target_name = m.group(1).replace('_', '/')
                                if re.match(r'.*modules', rel_root) or re.match(r'.*apps', rel_root):
                                    g_modules += [target_name.encode()]
                                elif re.match(r'.*libs', rel_root):
                                    g_libs += [target_name.encode()]

    g_libs.sort()
    g_modules.sort()


def clean_list(includes):
    new_include_list = []

    if len(includes) == 0:
        return new_include_list

    includes.sort(key=lambda s: s[1].lower())

    prev_module = includes[0][0]
    for module, include in includes:
        if prev_module.split(b'/')[0] != module.split(b'/')[0]:
            new_include_list += [b'\n']
        new_include_list += [include]
        prev_module = module

    new_include_list += [b'\n']
    return new_include_list


def is_include_in_list(include: str, include_list):
    for i in include_list:
        if include.startswith(i):
            return True
    return False


def sort_includes(path, enable_reformat):
    try:
        cur_lib = find_current_library(path)
    except RuntimeError:
        common.warn('Failed to find current library for file ' + path + ', includes order might be wrong.\n')
        cur_lib = '!!NOTFOUND!!'

    pathname = os.path.dirname(__file__) + "/"

    file = open(pathname + "std_headers.txt", 'rb')
    lib_std = file.read()
    file.close()

    file = open(path, 'rb')
    content = file.readlines()
    file.close()

    includes = set()
    first_line = -1
    last_line = -1

    out_of_include = False

    for i, line in enumerate(content):
        if re.match(b"#include", line):
            if out_of_include:
                common.warn(
                    'Failed to parse includes in file ' + path + ', include sort is skipped. Maybe there is a #ifdef ?'
                                                                 ' This may be handled in a future version.\n')
                return FormatReturn()

            if first_line == -1:
                first_line = i
            last_line = i

            includes.add(line)
        elif first_line > -1 and line != b'\n':
            out_of_include = True

    if first_line == -1 and last_line == -1:
        # No include, skip
        return FormatReturn()

    include_modules = []

    # Create associated list of modules
    for include in includes:
        include_path_match = re.match(b'.*<(.*/.*)>', include)
        module = ""
        if include_path_match:
            module = include_path_match.group(1)
        else:
            include_path_match = re.match(b'.*"(.*/.*)"', include)
            if include_path_match:
                module = include_path_match.group(1)
            else:
                module = b''

        include_modules += [module]

    own_header_include = []
    current_module_includes = []
    lib_includes = []
    modules_includes = []
    other_includes = []
    std_includes = []

    orig_path = re.sub(".hg-new", "", path)
    extension = os.path.splitext(orig_path)[1]

    cpp = False
    if extension == ".cpp":
        filename = os.path.basename(orig_path)
        matched_header = re.sub(extension, ".hpp", filename)
        cpp = True

    for include, module in zip(includes, include_modules):

        if cpp and re.search(b'".*' + matched_header.encode() + b'.*"', include):
            own_header_include += [(module, include)]
        elif not cpp and re.search(b'<\\w*/.*/config.hpp>', include):
            own_header_include += [(module, include)]
        elif module == cur_lib.encode() or re.search(b'".*"', include):
            current_module_includes += [(module, include)]
        elif is_include_in_list(module, g_libs):
            lib_includes += [(module, include)]
        elif is_include_in_list(module, g_modules):
            modules_includes += [(module, include)]
        else:
            include_path_match = re.match(b'.*<(.*)>', include)
            if include_path_match and include_path_match.group(1) in lib_std:
                std_includes += [(module, include)]
            else:
                other_includes += [(module, include)]

    new_includes = clean_list(own_header_include) + clean_list(current_module_includes) + clean_list(
        lib_includes) + clean_list(modules_includes) + clean_list(other_includes) + clean_list(std_includes)

    new_content = []
    for i, line in enumerate(content):
        if i == first_line:
            new_content += new_includes[:-1]
        elif i < first_line or i > last_line:
            new_content += [line]

    if content != new_content:
        if enable_reformat:
            open(path, 'wb').writelines(new_content)
            return FormatReturn(modified=True)
        else:
            common.error('Include headers are not correctly sorted in file : ' + path + '.')
            return FormatReturn(erroneous=True)

    return FormatReturn()
