#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""

Hooks checking if objects and services used in XML have their dependencies met.

[hooks]
pretxncommit.check_xml_dependencies = python:/path-to/hooks:check_xml_dependencies

"""

from xml.etree import ElementTree as ET

import common
import check_xml
import os
import os.path
import re


def cmakelists_parser(content, repo_name):
    """Parses a 'CMakeLists.txt" file.

    Args:
        content (str): the 'CMakeLists.txt' content.

    Returns:
        Tuple[str, List[str]]:  the target's 'TYPE' and its list of 'REQUIREMENTS'.

    """
    commentless_content = re.sub(r'#[^\n]*', '', content)  # Delete comments
    requirements_matches = re.search(
        r'.*add_dependencies\(\s*\w*\s+([^\)]*)\)*', commentless_content)
    depends_matches = re.findall(
        r'.*target_link_libraries\(\s*\w*\s+(?:PUBLIC|PRIVATE)?([^\)]*)\)*', commentless_content)
    type_matches = re.search(
        r'.*sight_add_target\(\s*\w*\s*.*TYPE\s*(\w*)', commentless_content)

    requirements = []
    target_type = 'UNDEFINED'

    if requirements_matches:
        requirements_txt = requirements_matches.group(1)
        requirements = requirements_txt.split()
        requirements = list(repo_name + "::" + req if not re.search(r'::', req) else req for req in requirements)
        requirements = list(map(lambda r: r.replace('_', '::'), requirements))
        # Hackish, but sufficient for now to process the exception...
        # The correct way would be to check the actual location of the module to determine its name and not
        # replace all '::' above, similar to what we do in the sight profile generator cmake script
        requirements = list(map(lambda r: r.replace('scene3d::qt', 'scene3d_qt'), requirements))

    depends = []

    for dep in depends_matches:
        depends += dep.split()
        depends = list(map(lambda r: r.replace('_', '::'), depends))

    if type_matches:
        target_type = type_matches.group(1)

    return target_type, requirements + depends


def get_service_module(service_config, requirements):
    """Returns true if a service type name matches one of the requirements.

    Args:
        service_config (xml.etree.ElementTree): the service's XML configuration.
        requirements (List[str]): requirements declared in the 'CMakeLists.txt'.

    Returns:
        Optional[str]:  the module to which the service belongs or `None` if it is not part of any.

    """
    service_type = service_config.attrib['type']

    if service_type[0] == '$':
        return None

    for r in requirements:
        if re.search(r'^' + r, service_type):
            return True

    # Known exceptions
    service_class = service_type.split('::')[-1]
    if (service_class == 'SRender' or service_class == 'SConfigController'
            or service_class == 'render' or service_class == 'config_controller'
            or service_class == 'multi_config_controller'):
        return True

    return False


def get_object_module(object_config):
    """Searches for the module required to declare the input object type.

    Args:
        object_config (xml.etree.ElementTree): the object's XML configuration.

    Returns:
        str:  the module required to declare the object.

    """
    object_type = object_config.get('type', None)
    if object_type is None:
        return None

    object_namespace = ''
    m = re.match(r'(?:::)?(\w*::)(module::)?(?:(\w*)::)?(?:(\w*)::)', object_type)
    if m:
        if m.group(2):
            if m.group(3):
                object_namespace = m.group(1) + 'module::' + m.group(3) + '::' + m.group(4)
            else:
                object_namespace = m.group(1) + 'module::' + m.group(4)
        else:
            if m.group(3):
                object_namespace = m.group(1) + m.group(3) + '::' + m.group(4)
            else:
                object_namespace = m.group(1) + m.group(4)
    else:
        namespace_and_type = object_type.split('::')
        object_namespace = namespace_and_type[0]

    # Map handling special cases
    special_cases_map = {
        'fwPacsIO': 'ioPacs',
        'fwRenderQt': 'scene2D'
    }

    default_registry = object_namespace

    return special_cases_map.get(object_namespace, default_registry)


def get_cmake_requirements(xml_file_path, repo_name):
    """Searches for the 'CMakeLists.txt' belonging to the xml file's module.

    Args:
        xml_file_path (os.path): the path to an XML configuration file.

    Returns:
        Tuple[bool, List[str]]: whether the target is a module or an app and the list of requirements from the
                                'CMakeLists.txt'.

    Raises:
        Exception: if no 'CMakeLists.txt' is found.

    """
    module_dir = os.path.split(os.path.abspath(os.path.dirname(xml_file_path)))[0]
    # Go down the parent directories to search for the 'CMakeLists.txt' file.
    while 'CMakeLists.txt' not in os.listdir(module_dir):
        parent_dir = os.path.split(os.path.join(module_dir))[0]

        parent_dir_list = os.listdir(parent_dir)
        # break when the repo root is reached
        if '.sight' in parent_dir_list:
            raise Exception('- configuration does not seem to be part of a module.\n')
        # break when the root directory is reached.
        elif module_dir == parent_dir:
            raise Exception('- configuration does not seem to be part of a repository.\n')

        module_dir = parent_dir

    cmakelists_path = os.path.join(module_dir, 'CMakeLists.txt')
    with open(cmakelists_path, 'rb') as content_file:
        cmake_content = content_file.read()

    target_type, requirements = cmakelists_parser(cmake_content.decode(), repo_name)
    is_app_or_module = target_type.upper() == 'BUNDLE' or target_type.upper() == 'MODULE' or target_type == 'APP'

    return is_app_or_module, requirements


def check_object_and_service_dependencies(config_tree, requirements):
    """Checks if all objects and services have their module included in the requirements.

    Args:
        config_tree (xml.etree.ElementTree): XML configuration tree.
        requirements (List[str]): requirements declared in the 'CMakeLists.txt'.

    Returns:
        str: error message with modules missing in the 'CMakeLists.txt', empty if dependencies are met.

    """
    service_types = config_tree.findall('./extension/config/service')
    service_types += config_tree.findall('./config/service')
    object_types = config_tree.findall('./extension/config/object')
    object_types += config_tree.findall('./config/object')

    object_namespaces = set(map(get_object_module, object_types))
    # Ignore objects that do no belong to a module
    object_namespaces = set(filter(lambda o: o is not None, object_namespaces))

    # Ignore objects and services that belong to the current config module
    if config_tree.tag == 'plugin' and config_tree.attrib['id'] is not None:
        requirements.append(str(config_tree.attrib['id']))

    err = ''
    for namespace in object_namespaces:
        if namespace not in requirements:
            err += '- an object from \'' + namespace + '\' is used but the module is missing from the ' \
                                                       '\'CMakeLists.txt\' requirements.\n'

    for service in service_types:
        result = get_service_module(service, requirements)
        if result is not None and not result:
            err += '- service of type \'' + service.attrib['type'] + '\' is used but the associated module is missing' \
                                                                     ' from the \'CMakeLists.txt\' requirements.\n'

    return err


def check_config_requirements(config_tree, requirements):
    """Parses all 'requirement' tags in the configuration.

    Checks if XML requirements are also in the requirements list.

    Args:
        config_tree (xml.etree.ElementTree): XML configuration tree.
        requirements (List[str]): requirements declared in the 'CMakeLists.txt'.

    Returns:
        str: error message with all the required modules missing in the 'CMakeLists.txt', empty if requirements are met.

    """
    config_requirements = config_tree.findall('./requirement')
    rq_ids = map(lambda r: r.attrib['id'], config_requirements)

    err = ''
    for rq in rq_ids:
        if rq not in requirements:
            err += '- configuration requires \'' + rq + \
                   '\' but it is missing in the \'CMakeLists.txt\'\n'

    return err


def check_xml_dependencies(files):
    """Parses all xml files to look for missing dependencies.

    Checks if all the objects and services have their dependencies declared in the 'CMakeLists.txt'.
    Checks if XML requirements are also the 'CMakeLists.txt'.

    Args:
        files (List[common.FileAtIndex]): files staged in git.

    Returns:
        bool: False if the hook succeeds, True otherwise.

    """
    abort = False

    repo_name = common.get_repo_name()

    for f in files:
        if f.path.lower().endswith(('.xml')):
            common.trace('Checking ' + str(f.path) + ' dependencies...')
            error_msg = ''
            try:
                is_app_or_module, requirements = get_cmake_requirements(f.path, repo_name)

                if is_app_or_module:
                    config_tree = check_xml.xml_parser(f.contents.decode())
                    error_msg += check_config_requirements(config_tree, requirements)
                    error_msg += check_object_and_service_dependencies(config_tree, requirements)

            except ET.ParseError as err:
                error_msg += err.msg
            except Exception as exception:
                error_msg += str(exception)

            if error_msg:
                common.error('Dependency missing in ' + f.path + ' :\n' + error_msg + '\n')
                abort = True

    return abort


hooks = {
    'check_xml_dependencies': check_xml_dependencies,
}
