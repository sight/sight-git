#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Cppcheck your code.

.gitconfig configuration :

[sight-hooks]
    hooks = cppcheck

[cppcheck-hook]
    source-patterns = *.cpp *.cxx *.c
    header-patterns = *.hpp *.hxx *.h
    cppcheck-path="C:/Program Files/Cppcheck/cppcheck.exe"
    report-mode = html
    warnings-as-errors = true
"""

import os
import subprocess
from fnmatch import fnmatch

import common

SEPARATOR = '%s\n' % ('-' * 79)
CPPCHECK_PATH = 'cppcheck'


# ------------------------------------------------------------------------------

# Can we run cppcheck ?
def check_cppcheck_install():
    try:
        p = subprocess.Popen([CPPCHECK_PATH, '--version'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        p.communicate()

        return p.wait() != 0
    except FileNotFoundError:
        return True


# ------------------------------------------------------------------------------

def cppcheck(files):
    source_patterns = common.get_option('cppcheck-hook.source-patterns', default='*.cpp *.cxx *.c *.cu').split()
    header_patterns = common.get_option('cppcheck-hook.header-patterns', default='*.hpp *.hxx *.h *.cuh').split()
    report_mode = common.get_option('cppcheck-hook.report-mode', default='console')
    warnings_as_errors_opt = common.get_option('cppcheck-hook.warnings-as-errors', default='false')
    if warnings_as_errors_opt == 'true':
        warnings_as_errors = True
    elif warnings_as_errors_opt == 'false':
        warnings_as_errors = False
    else:
        common.error('invalid warnings as errors option "{}" (supported values: true, false)'
                     .format(warnings_as_errors_opt))
        return True

    if report_mode not in ['console', 'html']:
        common.error('invalid report mode "{}" (supported values: console, html)'.format(report_mode))
        return True

    code_patterns = source_patterns + header_patterns

    global CPPCHECK_PATH

    if common.g_cppcheck_path_arg is not None and len(common.g_cppcheck_path_arg) > 0:
        CPPCHECK_PATH = common.g_cppcheck_path_arg
    else:
        CPPCHECK_PATH = common.get_option('cppcheck-hook.cppcheck-path', default=CPPCHECK_PATH, type='--path').strip()

    if check_cppcheck_install():
        common.error('Failed to launch cppcheck.')
        return True

    files_to_analyze = []
    repoRoot = common.get_repo_root()
    for f in files:
        if any(fnmatch(f.path.lower(), p) for p in code_patterns):
            files_to_analyze.append(os.path.join(repoRoot, f.path))

    if len(files_to_analyze) == 0:
        return False

    common.note('Checking with ' + CPPCHECK_PATH)

    maybe_warnings_as_errors = ['--error-exitcode=2'] if warnings_as_errors else []
    maybe_suppressed_warnings = ['--suppressions-list=.cppcheck/suppressed-warnings.txt'] \
        if os.path.exists('.cppcheck/suppressed-warnings.txt') else []
    maybe_defines = ['--include=.cppcheck/defines.hpp'] if os.path.exists('.cppcheck/defines.hpp') else []

    # Invoke cppcheck for source code files
    p = subprocess.Popen([CPPCHECK_PATH, *maybe_warnings_as_errors, '-I' + os.path.join(repoRoot, 'libs'),
                          '-I' + os.path.join(repoRoot, 'libs/core'), '-I' + os.path.join(repoRoot, 'modules'),
                          '--library=boost,cppunit,opencv2,posix,windows', '--force', '-j%s' % os.cpu_count(),
                          '--inline-suppr', ('-v' if report_mode == 'console' else '--xml'),
                          '--language=c++',
                          '--suppress=unmatchedSuppression',
                          '--suppress=unmatchedSuppression:*',
                          *maybe_suppressed_warnings,
                          '--enable=warning,style,performance,portability,information', '--quiet',
                          *maybe_defines, '-DCPPCHECK',
                          *files_to_analyze],
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = p.communicate()

    ok = (p.wait() == 0)
    if err is not None:
        if report_mode == 'console':
            if ok:
                common.warn('CppCheck warnings found:')
            else:
                common.error('CppCheck errors found:')
            print(err.decode())
        else:
            if ok:
                common.warn('CppCheck warnings found, please check the generated HTML report in cppcheck/index.html')
            else:
                common.error('CppCheck errors found, please check the generated HTML report in cppcheck/index.html')
            with open('cppcheck.xml', 'w') as file:
                file.write(err.decode())
            htmlreport = subprocess.Popen([os.path.join(os.path.dirname(CPPCHECK_PATH), 'cppcheck-htmlreport'),
                                          '--file=cppcheck.xml', '--report-dir=cppcheck'],
                                          stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
            htmlreport.communicate()
            htmlreport.wait()
    if out:
        print(out.decode())
    return not ok


hooks = {
    'cppcheck': cppcheck,
}
