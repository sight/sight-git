# CSPELL custom dictionaries

Add here custom dictionaries that cspell will load through sheldon.

## Description

### sight-dict.txt

Contains generic terms and keywords used for sight toolkit, mainly focused on C++ but works also on simple text.
It contains for instance name of our external libraries (boost, opengl, opencv, ...), some of our abreviation (rgb, rgba)
or company names (ircad, ...).

### cmake-dict.ignore

Work in progress dictionary to handle CMake keywords. Extension is `.ignore` to block loading of this file by cspell.py.

## How to update dictionary

1. Dictionaries are case insensitive so please add your word on lowercase
2. Make sure to reorder alphabetically the list

## Add a new dictionary

If needed we can add other dictionary than `sight-dict.txt` more specialized, an example is shown with `cmake-dict.ignore`
which is a work-in-progress dictionnary to handle CMake keywords.

