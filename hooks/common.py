#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import collections
# From command line arguments
import datetime
import fnmatch
import os
import re
import subprocess
from enum import Enum, auto

g_trace = False
g_cppcheck_path_arg = None
g_uncrustify_path_arg = None
g_glslang_validator_path = None
g_options = {}
g_enable_reformat = True


class FormatReturn:
    def __init__(self, erroneous=False, modified=False):
        self.erroneous = erroneous
        self.modified = modified

    def add(self, other):
        self.erroneous = self.erroneous or other.erroneous
        self.modified = self.modified or other.modified


class FileAtIndex(object):
    def __init__(self, contents, size, mode, sha1, status, path):
        self.contents = contents
        self.size = size
        self.mode = mode
        self.sha1 = sha1
        self.status = status
        self.path = path

    def fnmatch(self, pattern):
        basename = os.path.basename(self.path)
        return fnmatch.fnmatch(basename, pattern)


class LogLevel(Enum):
    NOTE = auto()
    TRACE = auto()
    WARNING = auto()
    ERROR = auto()


def _sheldon_print(msg, log_level: LogLevel = LogLevel.NOTE):
    if isinstance(msg, str):
        decoded = msg.strip()
    elif isinstance(msg, bytes):
        decoded = msg.strip().decode()
    else:
        decoded = str(msg.strip())

    if log_level is LogLevel.WARNING:
        decoded = '* [Warning] ' + decoded + ' *'
    elif log_level is LogLevel.ERROR:
        decoded = '*** [ERROR] ' + decoded + ' ***'
    else:
        decoded = '* [Sheldon] ' + decoded

    print(decoded)


def note(msg):
    _sheldon_print(msg, LogLevel.NOTE)


def trace(msg):
    if g_trace:
        _sheldon_print(msg, LogLevel.TRACE)


def error(msg):
    _sheldon_print(msg, LogLevel.ERROR)


def warn(msg):
    _sheldon_print(msg, LogLevel.WARNING)


def binary(s):
    """return true if a string is binary data"""
    return b'\0' in s


ExecutionResult = collections.namedtuple(
    'ExecutionResult',
    'status, out',
)


def get_repo_root():
    """
    Return the root path of a git repository. This is assumed to be set first by a call
    to set_repo_root()
    :return: path
    """
    return get_repo_root.repo_root


def set_repo_root(root: str):
    """
    Set the path of the current git repository. This takes as input a file or a directory and look recursively
    backwards a special hidden file called ".sight". This is more convenient to use this instead of using git-rev-parse
    because we can "simulate" multiple repositories in unit-tests.
    :param root: path used as initial value to look for a ".sight" file
    """
    anchor = os.path.splitdrive(os.path.abspath(root))[0] + os.sep
    while not os.path.exists(os.path.join(root, ".sight")):
        [root, _] = os.path.split(root)
        if root == anchor:
            raise FileNotFoundError('Could not find .sight file in this repository')

    get_repo_root.repo_root = os.path.normpath(root)


def get_repo_name():
    """
    Return the name of the repository as stated in the .sight file
    """
    # Find sight repository name
    root = get_repo_root()

    with open(os.path.join(root, ".sight"), "r+") as file:
        line = file.readline()
        if len(line) < 1:
            raise RuntimeError('.sight file is empty')
        repo_name = line.strip()
        return repo_name


def get_license(file_path):
    """
    Return the license type for a given file?
    :param file_path:
    :return: [ True|False, 'Name', 'Entity 1', 'Entity 2', ... ]: where the first boolean indicates a free license
    """
    if not os.path.exists(file_path):
        raise FileNotFoundError

    cur_path = os.path.normpath(os.path.dirname(file_path))

    repo_root = get_repo_root()
    if os.path.commonpath([repo_root, cur_path]) != repo_root:
        raise RuntimeError('Wrong call, the requested file is not located inside the project repository.')

    copyright_re = re.compile(r'Copyright \(C\) [0-9\-]* ([\w ]*)', re.MULTILINE)
    is_free_license = None
    while True:
        if os.path.exists(os.path.join(cur_path, 'COPYING.LESSER')):
            cur_path = os.path.join(cur_path, 'COPYING.LESSER')
            is_free_license = True
            break
        if os.path.exists(os.path.join(cur_path, 'LICENSE.txt')):
            cur_path = os.path.join(cur_path, 'LICENSE.txt')
            is_free_license = False
            break
        if cur_path == repo_root:
            raise RuntimeError("Unable to find a license file.")
            # is_free_license = False
            # break
        cur_path = os.path.join(cur_path, '..')
        cur_path = os.path.normpath(cur_path)

    # if is_free_license is None:
    #    return ['False', 'IRCAD', 'IHU']

    with open(cur_path, 'r') as source_file:
        license_content = source_file.read()

    # Extract software name
    match = re.search(r'([\w-]*) is: Copyright', license_content, re.MULTILINE)
    if match is None:
        raise RuntimeError('Could not extract software name from the license')
    name = match.group(1)

    # Extract entities from copyright lines
    m = copyright_re.findall(license_content)

    if m is None:
        raise RuntimeError('Copyright holder not found')

    m = list(map(lambda expr: expr.strip(), m))
    if is_free_license:
        m.remove('Free Software Foundation')

    return [is_free_license] + [name] + m[:]


def _get_git_commit_datetime(path):
    # Get the git modification date of the file
    result = execute_command('git log -1 --format=%ad --date=format:%Y-%m-%dT%H:%M:%S ' + path)

    if result.status != 0:
        warn(result.out.decode())
        return None

    try:
        # Parse the string back to a datetime object
        return datetime.datetime.strptime(result.out.decode().strip(), '%Y-%m-%dT%H:%M:%S')
    except Exception as e:
        warn(e.message)
        return None


def get_git_author_entity():
    try:
        result = execute_command('git config --get user.email ')
        # Parse the string back to a datetime object
        email = result.out.decode().strip()

        if '@ihu-strasbourg.eu' in email:
            return 'IHU Strasbourg'
        else:
            return 'IRCAD France'
    except Exception as e:
        warn(e)
        return None


def get_file_datetime(path, check_commits_date):
    try:
        modification_time = datetime.datetime.fromtimestamp(os.path.getmtime(path))
    except Exception as e:
        warn(e.message)

        modification_time = None

    if check_commits_date:
        git_datetime = _get_git_commit_datetime(path)

        # Use git modification time if it is valid and creation_time == modification_time
        if git_datetime is not None:
            return git_datetime

    # Use the modification time, if any
    if modification_time is not None:
        return modification_time

    # Otherwise use the system time
    return datetime.datetime.today()


def execute_command(proc, output=subprocess.PIPE):
    """ Runs a command. Can redirect the standard output.

    Args:
        proc (str): command string.
        output (int, optional): output file descriptor.

    Returns:
        ExecutionResult: return status and command output.

    """
    try:
        out = subprocess.run(proc.split(), check=True, stdout=output, stderr=subprocess.STDOUT).stdout
    except subprocess.CalledProcessError as e:
        return ExecutionResult(1, e.output)
    except FileNotFoundError:
        return ExecutionResult(1, proc.split()[0] + " not found!")
    except OSError as e:
        return ExecutionResult(1, e.message)

    return ExecutionResult(0, out)


def current_commit():
    if execute_command('git rev-parse --verify HEAD').status != 0:
        return '4b825dc642cb6eb9a060e54bf8d69288fbee4904'
    else:
        return 'HEAD'


def get_option(option, default, type=""):
    # Try the eventual command line provided configuration options first
    try:
        return g_options[option]
    except KeyError:
        pass
    # Then try the user config
    try:
        out = subprocess.check_output(('git config ' + type + ' ' + option).split()).strip()
        return out.decode()
    except subprocess.CalledProcessError:
        pass
    # Then try the repository config
    try:
        out = subprocess.check_output(('git config -f .sheldonconfig ' + type + ' ' + option).split()).strip()
        return out.decode()
    except subprocess.CalledProcessError:
        # Else return the default
        return default


def _contents(sha):
    result = execute_command('git show ' + sha)

    if result.status == 0:
        return result.out

    warn(result.out.decode())
    return ""


def _diff_index(rev):
    result = execute_command('git diff-index --cached -z --diff-filter=AM ' + rev)

    if result.status == 0:
        return result.out

    warn(result.out.decode())
    return ""


def _diff(rev, rev2):
    if not rev2:
        result = execute_command('git show --raw -z --diff-filter=AMR  --format=%h -m {}'.format(rev))
    else:
        result = execute_command('git diff --raw -z --diff-filter=AMR {} {}'.format(rev, rev2))

    if result.status == 0:
        return result.out

    warn(result.out.decode())
    return ""


def _size(sha):
    result = execute_command('git cat-file -s ' + sha)
    if result.status == 0:
        try:
            return int(result.out)
        except ValueError:
            return 0

    warn(result.out.decode())
    return 0


def files_in_rev(rev, rev2=''):
    # see: git help diff-index
    # "RAW OUTPUT FORMAT" section
    diff_row_regex = re.compile(
        b'''
        :
        (?P<old_mode>[^ ]+)
        [ ]
        (?P<new_mode>[^ ]+)
        [ ]
        (?P<old_sha1>[^ ]+)
        [ ]
        (?P<new_sha1>[^ ]+)
        [ ]
        (?P<status>[^\0]+)
        \0
        (?P<path>[^\0:]+)
        \0
        ((?P<new_path>[^\0:]+)
        \0)?
        ''',
        re.X
    )

    for match in diff_row_regex.finditer(_diff(rev, rev2)):
        mode, sha, status, path, new_path = match.group(
            'new_mode', 'new_sha1', 'status', 'path', 'new_path'
        )

        # We only care about the new file name
        if new_path:
            path = new_path

        # Excludes sub-module
        if mode and mode.decode() == '160000':
            note("Exclude item '{}' (submodule)".format(path.decode()))
            continue

        if status is None or status.decode() == 'D':
            continue

        # Try to guest if the file has been deleted in a later commit
        file_status = status_of_file(get_repo_root() + '/' + path.decode())

        if file_status is None or file_status == 'D':
            continue

        content = _contents(sha.decode())

        if content is None or len(content) <= 0:
            continue

        size = _size(sha.decode())

        if size is None or size <= 0:
            continue

        yield FileAtIndex(
            content,
            size,
            mode.decode(),
            sha.decode(),
            status.decode(),
            path.decode()
        )


def files_staged_for_commit(rev):
    # see: git help diff-index
    # "RAW OUTPUT FORMAT" section
    diff_index_row_regex = re.compile(
        b'''
        :
        (?P<old_mode>[^ ]+)
        [ ]
        (?P<new_mode>[^ ]+)
        [ ]
        (?P<old_sha1>[^ ]+)
        [ ]
        (?P<new_sha1>[^ ]+)
        [ ]
        (?P<status>[^\0]+)
        \0
        (?P<path>[^\0]+)
        \0
        ''',
        re.X
    )
    diff_idx = _diff_index(rev)
    for match in diff_index_row_regex.finditer(diff_idx):
        mode, sha, status, path = match.group(
            'new_mode', 'new_sha1', 'status', 'path'
        )

        # Try to guest if the file has been deleted in a later commit
        file_status = status_of_file(get_repo_root() + '/' + path.decode())

        if status is not None and status != 'D' and file_status is not None and file_status != 'D':
            yield FileAtIndex(
                _contents(sha.decode()),
                _size(sha.decode()),
                mode.decode(),
                sha.decode(),
                status.decode(),
                path.decode()
            )


def status_of_file(path):
    # By default status is set to 'A' like it is a new file.
    # if the git status is '??' or empty, we guess also that the file is a new file.
    status = 'A'
    gitstatus = execute_command('git status --porcelain ' + path)

    if gitstatus.status != 0:
        warn("File : " + path + " is not in a git repository, sheldon will consider it like a new file")
    else:
        out = gitstatus.out.split()

        # if out is not empty and not equal to '??' so we have modified a tracked file.
        if out and out[0] != '??':
            status = out[0]

    return status


def file_on_disk(path):
    status = status_of_file(path)

    if status is not None and status != 'D':
        with open(path, 'rb') as content_file:
            content = content_file.read()

        stat = os.stat(path)
        size = stat.st_size

        yield FileAtIndex(
            content,
            size,
            '',
            '',
            status,
            path
        )


def directory_on_disk(path):
    for root, dirs, files in os.walk(path):
        for name in files:
            file_path = os.path.join(root, name)
            yield next(file_on_disk(file_path))


def apply_sheldon_ignore(files: [FileAtIndex]):
    sheldonignores = []

    try:
        # read .sheldonignore file content
        with open(os.path.join(get_repo_root(), ".sheldonignore"), "r") as sheldonignore_file:
            for line in sheldonignore_file.readlines():
                # Ignore comment
                trimmed = (line.split(sep='#')[0]).strip()
                if len(trimmed) > 0:
                    sheldonignores.append(trimmed)
    except Exception:
        pass

    # If nothing should be ignored
    if len(sheldonignores) <= 0:
        return files

    def must_ignore(candidate: FileAtIndex):
        for sheldonignore in sheldonignores:
            # if this is an "absolute" path
            if sheldonignore[0] == '/':
                # then match if the file starts with the sheldonignore
                sheldonignore = sheldonignore.lstrip('/') + "*"
                if re.compile(r"^" + fnmatch.translate(sheldonignore)).match(candidate.path):
                    return True
            else:
                # otherwise match if the file contains the sheldonignore
                sheldonignore = "*" + sheldonignore + "*"
                if fnmatch.fnmatchcase(candidate.path, sheldonignore):
                    return True
        return False

    return [f for f in files if not must_ignore(f)]
