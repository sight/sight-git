#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Apply cspell to your code.

"""
import fnmatch
import json
import os
import re
import shutil
import subprocess
import tempfile

import common


# On Windows, we need to be more precise about the executable to be found
# Otherwise false positives will occur
if os.name == 'nt':
    cspell_bin = "cspell.cmd"
else:
    cspell_bin = "cspell"

# ------------------------------------------------------------------------------


# Can we run cspell ?
def check_cspell_install():
    out = shutil.which(cspell_bin)

    return out is None


# ------------------------------------------------------------------------------


# Return True if cspell found errors in specified file
def check_file(files_to_process, add_dict_paths=None):

    if add_dict_paths is None:
        add_dict_paths = [os.path.join(common.get_repo_root(), ".cspell")]
    elif not isinstance(add_dict_paths, list):
        add_dict_paths = [add_dict_paths]

    # Paths to be checked for word dictionaries
    dict_paths = [os.path.join(os.path.dirname(__file__), ".cspell")]
    dict_paths.extend(add_dict_paths)

    # List storing all the dictionary files found
    dict_files = []

    # Iterate over the input files to find .txt dictionaries
    for dp in dict_paths:
        if os.path.exists(dp):
            for root, dirs, files in os.walk(dp):
                for name in files:
                    if name.endswith(".txt"):
                        dict_files.append(os.path.join(root, name))

    common.note('Using the following dictionaries: ')
    for d in dict_files:
        common.note('- ' + d)

    # Build the cspell configuration using the found dictionaries
    cspell_config = dict()
    cspell_config["version"] = "0.2"
    cspell_config["allowCompoundWords"] = True
    # Initialize with a bunch of useful dictionaries
    cspell_config["dictionaries"] = ["cpp", "python",
                                     "powershell", "bash", "lorem-ipsum", "en-gb", "markdown"]
    cspell_config["dictionaryDefinitions"] = []

    # Iterate over the dictionary files found
    # to build up a cspell configuration
    for i, d in enumerate(dict_files):
        # Update the dictionaries entry
        cspell_config["dictionaries"].append(str(i))

        # Add a new dictionary definition
        new_dict = dict()
        new_dict["name"] = str(i)
        new_dict["path"] = str(d)
        new_dict["addWords"] = True
        cspell_config["dictionaryDefinitions"].append(new_dict)

    # Create a temporary folder for the configuration
    with tempfile.TemporaryDirectory() as tmpdir:
        # Save the cspell configuration in the same directory
        cfg_path = os.path.join(tmpdir, "cspell.json")

        cfg_file = open(cfg_path, "w")
        json.dump(cspell_config, cfg_file, indent=4)
        cfg_file.close()

        files_per_line = "\n".join(files_to_process)

        command = [shutil.which(cspell_bin),
                   "lint",
                   "--no-must-find-files",
                   "--no-progress",
                   "--config", cfg_path,
                   "--file-list", "stdin"]

        p = subprocess.Popen(command,
                             stdout=subprocess.PIPE,
                             stdin=subprocess.PIPE,
                             stderr=subprocess.STDOUT)
        out, err = p.communicate(input=files_per_line.encode())

        if err is not None:
            common.error('Cspell failure: ', err)
            return True

        if out is not None:
            out = out.decode()
            for line in out.splitlines():
                common.note(line)

        if p.wait() != 0:
            common.error('Cspell failure')
            return True

    return False


# ------------------------------------------------------------------------------


def cspell(files):
    if check_cspell_install():
        common.error('Failed to launch cspell.')
        common.error('Please follow the installation guide on '
                     + 'https://cspell.org/ to install it.')
        return True

    repo_root = common.get_repo_root()

    # Build the magic regex
    patterns = ["*.cpp", "*.cxx", "*.c", "*.hpp", "*.hxx", "*.h", "*.glsl", "*.cu", "*.py", "*.bat", "*.sh", "*.md"]
    file_matcher = re.compile('|'.join(fnmatch.translate(p) for p in patterns))

    # List the files to process
    files_to_process = [os.path.join(repo_root, file.path) for file in files if file_matcher.match(file.path)]

    # Only do something when needed
    if len(files_to_process) > 0:
        # For now, cspell does not trigger a sheldon error
        return check_file(files_to_process)

    return False


hooks = {
    'cspell': cspell
}
