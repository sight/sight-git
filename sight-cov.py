#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pathlib import Path
import argparse
import os
import re
import shutil
import subprocess
import tempfile
import textwrap

import gitlab.v4.objects
from pycobertura import Cobertura, TextReporterDelta
from pycobertura.filesystem import filesystem_factory
from pycobertura.reporters import (
    HtmlReporterDelta
)


def download_job_artifacts(_project: gitlab.v4.objects.projects.Project, _job_id: str, _output_dir: Path):
    """
    Download the build artifacts, stored as a zip file, for a given job.
    The zip file always contains a .build directory as root.
    :param _project: project from which to download artifacts
    :param _job_id: unique id of the job
    :param _output_dir: directory where to extract the zip file
    """
    with tempfile.TemporaryDirectory() as tmp:
        tmp_dir = Path(tmp)
        artifacts = tmp_dir / "__artifacts.zip"
        with open(artifacts, "wb") as f:
            job = _project.jobs.get(_job_id)
            job.artifacts(streamed=True, action=f.write)
        shutil.unpack_archive(filename=artifacts, extract_dir=tmp_dir)
        artifacts.unlink()
        if _output_dir.exists():
            shutil.rmtree(_output_dir)
        target_dir = tmp_dir / '.build'
        shutil.copytree(target_dir, _output_dir)
        shutil.rmtree(target_dir)


def get_coverage_job(_pipeline: gitlab.v4.objects.pipelines.ProjectPipeline):
    """
    Try to find a job that computes the code coverage. Depending
    on the repository, the job has a different name
    :param _pipeline: unique id of the pipeline
    :return: pipeline job if found, None otherwise
    """
    coverage_job_re = r'.*linux-debug-gcc' if args.repository == "night" else r'.*coverage-gcc'
    for pipeline_job in _pipeline.jobs.list():
        job_match = re.match(coverage_job_re, pipeline_job.name)
        if job_match and pipeline_job.stage == 'build':
            return pipeline_job

    return None


parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='Download the code coverage report from the latest pipeline of a merge-request.',
    epilog=textwrap.dedent('It is also possible to compare the coverage with the dev branch thanks to pycobertura.')
)

parser.add_argument('repository',
                    help='Name of the repository: "sight", "night, or a private repository.')

parser.add_argument('merge_request',
                    metavar='merge-request',
                    help='Identifier of the merge-request, same as the one present in its URL.')

parser.add_argument('-d', '--diff',
                    action="store_true",
                    help='Compute the diff of the coverage with the dev branch.')

parser.add_argument('--html',
                    action="store_true",
                    help='Html output instead of text.')

parser.add_argument('--verbose',
                    action='store_true',
                    help='Verbose mode')

args = parser.parse_args()

config_path = Path('~').expanduser() / '.python-gitlab.cfg'
if not config_path.exists():
    raise RuntimeError("Python Gitlab configuration not found, please configure your $HOME/.python-gitlab.cfg file "
                       "as described in the README.")

gl = gitlab.Gitlab.from_config('ircad.fr', [config_path])

if not (Path().cwd() / '.git').exists():
    print('Current repository does not seem to be a git repository')
    exit(1)

project = None
if args.repository in ["sight", "night"]:
    project = gl.projects.get("sight/" + args.repository)
else:
    projects = gl.projects.list(search=args.repository)
    project = list(filter(lambda x: x.path == args.repository and not hasattr(x, 'forked_from_project'), projects))[0]

try:
    mr = project.mergerequests.get(args.merge_request)
except gitlab.exceptions.GitlabGetError:
    print(f"Unable to found the merge-request !{args.merge_request}")
    exit(1)

if args.verbose:
    print(f'Merge-request #{mr.iid}: {mr.title}')
    print(f'Author: {mr.author["name"]}')
    print(f'Pipeline: #{mr.pipeline["id"]}, status: {mr.pipeline["status"]}, sha: {mr.pipeline["sha"]}')
    print(f'Source branch: {mr.source_branch}')
    print(f'head sha: {mr.diff_refs["head_sha"]}')
    print(f'Target branch: {mr.target_branch}')
    print(f'base sha: {mr.diff_refs["base_sha"]}\n')

mr_base_sha = mr.diff_refs['base_sha']
mr_pipeline_id = mr.pipeline['id']

pipeline = project.pipelines.get(mr_pipeline_id)
pipeline_job = get_coverage_job(pipeline)
if pipeline_job is None:
    print(f'Cannot find a coverage job in pipeline #{mr_pipeline_id}')
    exit(1)

if pipeline_job.status != "success":
    print(f'Last coverage job #{pipeline_job.id} is not yet successful.')
    print(f'Current state: {pipeline_job.status} in pipeline #{mr_pipeline_id}. Cannot proceed')
    exit(1)

coverage_output = Path().cwd() / '.coverage'
coverage_output.mkdir(exist_ok=True)

print(f'Downloading artifacts of merge-request !{args.merge_request} from project "{args.repository}"...')
mr_artifacts_dir = coverage_output / f'{args.repository}_mr{args.merge_request}_pipeline{mr_pipeline_id}'
download_job_artifacts(project, pipeline_job.id, mr_artifacts_dir)

url = mr_artifacts_dir / 'coverage' / 'index.html'
print('Coverage report is available at:')
print(f' file://{url}\n')

if args.diff:
    # Download sight coverage
    dev_artifacts_dir = coverage_output / f'{args.repository}-artifacts'

    for current in project.pipelines.list(ref='dev', iterator=True):
        current_pipeline_job = get_coverage_job(current)
        if current.sha == mr_base_sha and current_pipeline_job is not None:
            pipeline = current
            pipeline_job = current_pipeline_job
            print('Select dev pipeline ', end='')
            print(f' #{pipeline.id}, status: {pipeline.status}, commit: {pipeline.sha}, job: {pipeline_job.id}')
            break
        if args.verbose:
            print(f'Skip pipeline #{current.id}, status: {current.status}, commit: {current.sha}')
    else:
        print('Could not find any successful coverage build for dev branch, can not download artifacts')
        exit(1)

    print(f'\nDownloading dev branch artifacts of project "{args.repository}"')
    download_job_artifacts(project, pipeline_job.id, dev_artifacts_dir)

    print('Coverage report is available at:')
    print(f" file://{dev_artifacts_dir / 'coverage' / 'index.html'}\n")

    cobertura_xml_path = Path('coverage', 'cobertura-coverage.xml')
    dev_coverage_file = dev_artifacts_dir / cobertura_xml_path

    with tempfile.TemporaryDirectory() as sources_dir_temp:
        git_env = dict(os.environ, GIT_LFS_SKIP_SMUDGE="1")

        source_dev = Path(sources_dir_temp, 'source_dev')
        git_cmd = ["git", "clone", "-q"]
        subprocess.run(git_cmd + [project.http_url_to_repo, source_dev], env=git_env)
        subprocess.run(['git', 'checkout', '-q', mr_base_sha], cwd=source_dev, env=git_env)

        git_cmd = ["git", "clone", "-q", "--depth", "1"]
        source_mr = Path(sources_dir_temp, 'source_mr')
        subprocess.run(git_cmd + ["--branch", mr.source_branch, project.http_url_to_repo, source_mr], env=git_env)

        filesystem_dev = filesystem_factory(source_dev)
        coverage_dev = Cobertura(dev_coverage_file, filesystem_dev)

        filesystem_mr = filesystem_factory(source_mr)
        coverage_mr = Cobertura(mr_artifacts_dir / cobertura_xml_path, filesystem_mr)
        if args.html:
            delta = HtmlReporterDelta(coverage_dev, coverage_mr)
            report_output = Path('.coverage', 'report.html')
            with open(report_output, 'w', encoding='utf-8') as file:
                file.writelines(delta.generate())
                print('Diff coverage report is available at:')
                print(f' file://{Path().cwd() / report_output}')
        else:
            delta = TextReporterDelta(coverage_dev, coverage_mr)
            print(delta.generate())
