# Git tools to manage contributions with Sight

## Sheldon

### Requirements

* [Python 3.x](https://www.python.org/downloads/)
* [Git](https://git-scm.com/)
* To use *codingstyle-hook*, you must install **[Uncrustify-0.72](https://github.com/uncrustify/uncrustify)**
* To use *cppcheck-hook*, you must install [CppCheck](http://cppcheck.sourceforge.net/)
* To use *glslang-validator-hook*, you must install [glslang](https://github.com/KhronosGroup/glslang/releases/)
* To use *clang-tidy-hook*, you must install [Clang-Tidy](https://clang.llvm.org/extra/clang-tidy/)
* To use *cmake-format*, you must install [cmake-format](https://github.com/cheshirekow/cmake_format)
* To use *prettier*, you must install [prettier-xml](https://github.com/prettier/plugin-xml)
* To use *cspell*, you must install [cspell](https://github.com/streetsidesoftware/cspell)

Linux:
```commandline
# install packages
sudo apt-get install -y python3 python3-pip npm uncrustify cppcheck glslang-tool clang-tidy cmake-format

# install nodejs stable > 14 and make it as default
sudo apt-get install -y npm
sudo npm install -g n
sudo n stable
hash -r

# install cspell, prettier and prettier xml plugin
sudo npm install --global cspell prettier @prettier/plugin-xml
```

Windows (administrator console):
* To install other required software, you can use chocolatey GUI or use UniGetUI.
  * Install Chocolatey from website https://chocolatey.org/install or using *winget install "Chocolatey.Chocolatey"*
  * Or install UniGetUI, a multi-backend package manager, from https://www.marticliment.com/unigetui/ or using *winget install MartiCliment.UniGetUI*

```commandline
choco install node-js python3 llvm cppcheck 
choco install uncrustify --version=0.72
pip3 install cmake-format
npm install --global cspell prettier @prettier/plugin-xml
```

### Presentation

Sheldon is a tool that allows to check that files or commits respect our [coding guidelines](https://sight.pages.ircad.fr/sight-doc/CodingStyle/index.html)

```commandline
usage: sheldon [-h] [-f] [-v] [--with-uncrustify UNCRUSTIFY_PATH]
               [--with-cppcheck CPPCHECK_PATH]
               [--with-glslang-validator GLSLANG_VALIDATOR_PATH]
               [-i INPUT_PATH] [-c COMMIT_CHECK] [-o OPTIONS] [--hooks HOOKS]
               [path ...]

Check and/or reformat code to comply to Sight coding guidelines.

positional arguments:
  path                  Git path, can be a commit or two commits.

options:
  -h, --help            show this help message and exit
  -f, --format          Enable code reformatting.
  -v, --verbose         Increase the verbosity level.
  --with-uncrustify UNCRUSTIFY_PATH
                        Use uncrustify from path.
  --with-cppcheck CPPCHECK_PATH
                        Use cppcheck from path.
  --with-glslang-validator GLSLANG_VALIDATOR_PATH
                        Use glslang from path.
  -i INPUT_PATH, --input INPUT_PATH
                        Check the specific file/directory, staged or not.
                        Recursive when the argument is a directory
  -c COMMIT_CHECK, --commit COMMIT_CHECK
                        Check all unpushed commit
  -o OPTIONS, --config OPTIONS
                        Override configuration options in the KEY=VALUE
                        format. May be specified multiple times.
  --hooks HOOKS         The hooks to be run. Overrides the hooks specified in
                        the config file. Available hooks: crlf, cr, tab, lgpl,
                        bsd, doxygen, badwords, filesize, check_xml,
                        check_xml_dependencies, cppcheck, glslang_validator,
                        check_glsl_version, cspell, clang-tidy, clang-tidy-fix

The script works on git staged/modified files or directly on file/directory:

  For git mode, in three different ways depending on the number of paths:
    - If no path is specified, the current staged files are processed.
    - If 1 path is specified, the files modified in the specified path is processed.
    - If 2 paths are specified, the files modified between the two paths are processed.

  For file/directory mode, using the --input argument:
    - If the argument is a file, only this file will be checked.
    - If the argument is a directory, Sheldon will recursively check all files within this directory.
```

### Examples

**Example 1:**

```commandline
vim main.cpp
git add main.cpp
sheldon -f
```

checks and modifies the file `main.cpp` to comply to our rules.

**Example 2:**

```commandline
sheldon 511c628^!
```

check files modified in the commit 511c628.

**Example 3:**

```commandline
sheldon 124e8415 511c628
```

check files modified between commits 124e8415 and 511c628.

**Example 4:**

```commandline
sheldon -i main.cpp
```

checks the file `main.cpp` (current local version).

### Configuration

Sheldon configuration is stored in git config files, so you can have global,
user or repository specific settings.

**Available options are:**

- **codingstyle-hook.uncrustify-path**: path to the uncrustify binary (default to `uncrustify`, which means it should be in the global `PATH`)
- **codingstyle-hook.source-patterns**: list of files extensions matching source files (default: `*.cpp *.cxx *.c`)
- **codingstyle-hook.header-patterns**: list of files extensions matching header files (default: `*.hpp *.hxx *.h`)
- **codingstyle-hook.cmake-patterns**: list of files extensions matching cmake files (default: `*CMakeLists*.txt *.cmake`)
- **codingstyle-hook.misc-patterns**: list of files extensions matching other files (default: `*.xml *.json`)
- **codingstyle-hook.sort-includes**: enable or disable header includes sort (default: `true`)
- **codingstyle-hook.additional-projects**: list of additional **Sight** repositories paths used to sort includes (separated with a ;). (default: parent folder of the current repository)
- **cppcheck-hook.cppcheck-path**: path to the cppcheck binary (default to `cppcheck`, which means it should be in the global `PATH`)
- **cppcheck-hook.source-patterns**: list of files extensions matching source files (default: `*.cpp *.cxx *.c`)
- **cppcheck-hook.header-patterns**: list of files extensions matching header files (default: `*.hpp *.hxx *.h`)
- **cppcheck-hook.report-mode**: format of the report created. Supported values: `console` (the output is printed on the console), `html` (the output is written in an HTML file). Only one report format can be specified (default: `console`)
- **cppcheck-hook.warnings-as-errors**: if `true`, this hook will fail when CppCheck finds an error (default: `false`)
- **sight-hooks.hooks**: list of enables hooks amongst `crlf cr tab lgpl bsd doxygen badwords filesize check_xml check_xml_dependencies cppcheck glslang_validator check_glsl_version cspell clang-tidy clang-tidy-fix`
- **forbidtoken-hooks.crlf**: list of file extensions that should be checked for
the end of line hook (default: `*.cpp *.hpp *.hxx *.cxx *.c *.h *.xml *.txt *.cmake *.py`)
- **forbidtoken-hooks.lgpl**: list of file extensions that should be checked for
the lgpl hook  (default: `*.cpp *.hpp *.hxx *.cxx *.c *.h *.xml *.txt *.cmake`)
- **forbidtoken-hooks.tab**: list of file extensions that should be checked for
the tab hook  (default: `*.cpp *.hpp *.hxx *.cxx *.c *.h *.xml *.txt *.cmake *.py`)
- **forbidtoken-hooks.digraphs**: list of file extensions that should be checked for
the digraphs hook (default: `*.cpp *.hpp *.hxx *.cxx *.c *.h`)
- **forbidtoken-hooks.doxygen**: list of file extensions that should be checked for
the doyxgen hook (default: `*.cpp *.hpp *.hxx *.cxx *.c *.h`)
- **forbidtoken-hooks.badwords**: list of file extensions that should be checked for
the copain hook (default: `*.cpp *.hpp *.hxx *.cxx *.c *.h`)
- **filesize-hook.max-size**: set the maximum size of files (default 1048576)
- **filesize-hook.type**: `binary` or `all` (default `all`)
- **glslang-validator-hook.glslang-validator-path**: path to the glslangValidator binary (default to `glslangValidator`, which means it should be in the global `PATH`)
- **clang-tidy-hook.source-patterns**: list of files extensions matching source files (default: `*.cpp *.cxx *.c`)
- **clang-tidy-hook.header-patterns**: list of files extensions matching header files (default: `*.hpp *.hxx *.h`)
- **clang-tidy-hook.generate-compilation-db**: when the compilation database must be generated. Supported values: `never` (the hook won't generate the compilation database, and will instead fail if it doesn't exist), `when_absent` (the hook will generate the compilation database if it doesn't exist), `always` (the hook will  always generate the compilation database). when_absent and always requires clang-tidy-hook.build-path to be set. (default: `never`)
- **clang-tidy-hook.build-path**: the build folder to use when generating the compilation database if clang-tidy-hook.generate-compilation-db is either when_absent or always.
- **clang-tidy-hook.report-modes**: the report format(s) to be created. Supported values: `console` (the output is printed on the console), `log` (the output is written in a  plain text file), `html` (the output is written in an HTML file (requires *aha* to be installed)), `codequality` (the output is written in a JSON file in a format suitable for GitLab's CodeQuality report). Multiple report formats can be specified, they must be separated with a comma (`,`). (default: `console`)

Thus to change the path to uncrustify globally, you may call something like:
```commandline
git config --global codingstyle-hook.uncrustify-path /home/toto/software/uncrustify/bin/uncrustify
```

Allowlists can be provided to the spell-checker on a repository basis. To do so, create a `.cspell` folder at the root of the repository.    
All the `.txt` files contained in this folder will be parsed for words to be allowed in code.

### Unit-tests

- To execute unit-tests you must be in `hooks` folder.
- You can run a specific unit-tests (`test_check_xml` by example) with the command:

```commandline
> python3 -m unittest tests.test_check_xml
```

- To run all unit-tests, you can use `discover` option:

```commandline
> python3 -m unittest discover
```

### Configuration via git hooks

You can also use sheldon in specific hooks of your git repositories, via the `.git/hooks` directory located at the root of your repository.

Here are some examples of hooks you can configure:

- pre-commit: this hook will prevent a commit if the code you want to commit does not respect the guidelines.

```bash
#!/bin/sh

sheldon
```

For the hooks to be enabled, the files must be made executable.

## sight-cov

This utility downloads and extracts the code coverage report from the latest pipeline of a merge-request.

```commandline
usage: sight-cov.py [-h] [-d] [--html] [--verbose] repository merge-request

Download the code coverage report from the latest pipeline of a merge-request.

positional arguments:
  repository     Name of the repository: "sight", "night, or a private repository.
  merge-request  Identifier of the merge-request, same as the one present in its URL.

options:
  -h, --help     show this help message and exit
  -d, --diff     Compute the diff of the coverage with the dev branch.
  --html         Html output instead of text.
  --verbose      Verbose mode
```

For example:

```commandline
$ D:\Dev\src\sight-git\sight-cov.py -d sight 1200 --html 
Downloading artifacts of merge-request !1200 from project "sight"...
Coverage report is available at:
 file://D:\Dev\src\disrumpere\sight\.coverage\sight_mr1200_pipeline315435\coverage\index.html

Select dev pipeline  #315296, status: success, commit: bae0d6d0234c991368e5ed7b257af9d987d3ad94, job: 1075620

Downloading dev branch artifacts of project "sight"
Coverage report is available at:
 file://D:\Dev\src\disrumpere\sight\.coverage\sight-artifacts\coverage\index.html

Diff coverage report is available at:
 file://D:\Dev\src\disrumpere\sight\.coverage\report.html
```

To access private repositories or to configure custom CA certificate, specific configuration is required.
For python-gitlab to work, a configuration file `.python-gitlab.cfg` is needed in your home repository:

```ini
[global]
default = ircad.fr
ssl_verify = <PATH_TO_CUSTOM_CERTIFICATE>
timeout = 10

[ircad.fr]
url = https://git.ircad.fr
private_token = <YOUR_GITLAB_TOKEN_THERE>
api_version = 4
```
