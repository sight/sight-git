import string
from pathlib import Path
import argparse

parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='Analyse the difference in disk usage between the two given directories.\n'
                'By default, it will compare the contents of two directories recursively.',
    epilog='Example: du_diff.py --dir1=dir1 --dir2=dir2 [--files] [--ignore-size=1]'
)

parser.add_argument('--dir1',
                    metavar='dir1',
                    help='First directory to compare (the new directory).')

parser.add_argument('--dir2',
                    metavar='dir2',
                    help='Second directory to compare (the previous directory).')

parser.add_argument('--files',
                    action='store_true',
                    help='Report only diff at directory level')

parser.add_argument('--ignore-size',
                    metavar='ignore-size',
                    default=1,
                    help='Ignore diff less than given MB'
                    )
args = parser.parse_args()

if not args.dir1 or not args.dir2:
    print('Please provide two directories to compare')
    exit(1)

dir1 = Path(args.dir1)
dir2 = Path(args.dir2)
ignore_diff_size_less = int(args.ignore_size)


def _build_du_directories(directory: Path):
    du_dir = {}
    for root, dirs, files in directory.walk():
        files_size = round(sum((root / file).stat().st_size for file in files) / (1024 * 1024), 2)
        du_dir[root.relative_to(directory)] = files_size
    return du_dir


def _build_du_files(directory: Path):
    du_dir = {}
    for root, dirs, files in directory.walk():
        for file in files:
            path_filename = root / file
            file_size = round(path_filename.stat().st_size / (1024 * 1024), 2)
            du_dir[path_filename.relative_to(directory)] = file_size
    return du_dir


def build_du_for_directory(directory: Path):
    return _build_du_files(directory) if args.files else _build_du_directories(directory)


def build_report(directories, database):
    report = {}
    for element in sorted(directories):
        report[element] = database[element]
    return report


def intersection_report(report):
    sorted_report = dict(sorted(report.items(), key=lambda item: abs(item[1]), reverse=True))
    for (element, size_diff) in sorted_report.items():
        if abs(size_diff) < ignore_diff_size_less:
            continue
        if size_diff > 0:
            print(f'{element} bigger: {size_diff} MB')
        elif size_diff < 0:
            print(f'{element} smaller: {size_diff} MB')
        else:
            pass


def print_report(report: dict, message: string):
    sorted_report = dict(sorted(report.items(), key=lambda item: abs(item[1]), reverse=True))
    for (element, size_diff) in sorted_report.items():
        if abs(size_diff) < ignore_diff_size_less:
            continue
        print(f'{message}: {element} size: {size_diff} MB')


du_dir1 = build_du_for_directory(dir1)
du_dir2 = build_du_for_directory(dir2)

elements_dir1 = set(du_dir1.keys())
elements_dir2 = set(du_dir2.keys())

intersection = elements_dir1 & elements_dir2
only_dir1 = elements_dir1 - elements_dir2
only_dir2 = elements_dir2 - elements_dir1

report = {}
for element in sorted(intersection):
    size1 = du_dir1[element]
    size2 = du_dir2[element]
    diff = round(size1 - size2, 2)
    report[element] = diff

intersection_report(report)

report = build_report(only_dir1, du_dir1)
print_report(report, 'new element')

report = build_report(only_dir2, du_dir2)

print_report(report, 'removed element')
